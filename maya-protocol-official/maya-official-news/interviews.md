---
description: Watch Aaluxx speak about Maya with various hosts!
---

# Interviews

**March 13, 2023**

["Maya Protocol - Non-custodial Cross-chain swaps on Kujira" by Just Crypto](https://www.youtube.com/watch?v=vDGwNS4fZZw)

**March 6, 2023**

["Will DASH Plus MAYA Equal DEX Success? | Incubator WEEKLY" by Dash Incubator](https://www.youtube.com/watch?v=PYZzfBQhfXI)

**March 3, 2023**

["Halborn Flash Videos with Aaluxx Myth of Maya Protocol" by _Halborn Security_ ](https://www.youtube.com/watch?v=TPFPMlG8uvw)

**February 27, 2023**

["Maya Protocol Launch - Interview With AaluxxMyth About The Launch And Long-Term Plans" _by Kyle Krason_](https://www.youtube.com/watch?v=AJbU2fP7BEg)

**February 21, 2023**

["Citizen Cosmos: A citizen odyssey, ep. XVI, special mission - Maya Protocol" by Cosmos Citizen](https://www.youtube.com/watch?v=buYzwPmynOA)

**February 8, 2023**

["An Intro To... Maya Protocol!" by _Kuji Kast_](https://www.youtube.com/watch?v=hKs52EB6N9M)

**January 18, 2023**

["Aaluxx of the Maya Protocol on Expanding THORChain's Vision of Cross-Chain Swaps and Liquidity" _by Digital Cash Network_](https://www.youtube.com/watch?v=grLlQo5JdyY)

**October 13, 2022**

[**"**What is Maya Protocol?" _by Not Another Crypto Show_](https://www.youtube.com/watch?v=khpcqM2mLO0)

**December 19th, 2022**

["CROSS CHAIN CRYPTO EXPLOSION!? With Maya Protocol" _by Degens for a Brighter Future_](https://www.youtube.com/watch?v=2tG7l4UUUt8)
