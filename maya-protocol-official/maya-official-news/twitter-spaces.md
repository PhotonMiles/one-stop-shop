# Twitter Spaces

## **Mondays with Maya**

**#15:** [https://twitter.com/Maya\_Protocol/status/1650545334320345107](https://twitter.com/Maya\_Protocol/status/1650545334320345107)

<details>

<summary>Key points</summary>

* We've finally achieved a stable version of the network with an incredible 2,000 swaps and an astounding $1 million of accumulated volume in just 4 days! This is only the beginning, and we're already looking forward to our next milestone: 10 million!
* Right now, we're focused on increasing the number of liquidity nodes - the more nodes we have, the more decentralized we become, and the deeper liquidity we have. This will also provide $CACAO with greater stability against any potential withdrawal activity, which has been surprisingly low - a true testament to the confidence in the protocol!
* Maya will integrate $DASH in the next major update, which will create swap routes that are unique to Maya Protocol. This integration will provide more use cases and higher volume, making Maya a more attractive option.
* Mid-term, we will work on other integrations such as KUJI, ADA (might happen earlier than expected), and $AZTEC.
* ThorWallet now enables you to swap all assets in the Web App, and you can now make swaps on mobile via Test Flight and Android.
* The Thorwallet team is working hard to enable LP. We expect this by the end of the week. We are extending the $MAYA AirDrop claiming process for $RUNE holders to four days after ThorWallet enables LP.
* Listing soon: We filling all applications for the CoinGecko listing team for review. We have already listed ourselves on Coinmarketcap. We just need to link the price info.



</details>

**#14:** [https://twitter.com/Maya\_Protocol/status/1647978356644651008](https://twitter.com/Maya\_Protocol/status/1647978356644651008)

<details>

<summary>Key points</summary>

* We are thrilled to announce that we have successfully donated CACAO! We have an exciting week ahead of us as we eagerly await the activation of trading. We will quickly churn and then we will be ready to unhault the trading.
* MAYA will concentrate on three pillars: Deep Liquidity, Routes (Chains), and Availability. Once trading is enabled, we will focus on onboarding integrations and partnerships, increasing swaps, and boosting volume. These three pillars must be of the highest quality.
* The team expects Rango to smoothly integrate Maya once trading is enabled, as ThorChain is already integrated into Rango. Rango is a Crosschain Dex/ Bridge aggregator that enables interoperability between 53 different blockchains.
* Until 20 April, $Rune holders can still claim the $Maya Airdrop. Once ThorChain reopens the pools, the claiming process will be simplified and the alpha airdrop will occur 10 days later.

</details>

**#13:** [https://twitter.com/Maya\_Protocol/status/1645449202141364224](https://twitter.com/Maya\_Protocol/status/1645449202141364224)

<details>

<summary>Key points</summary>

* We expect the final stage net to be available on Tuesday, which will include an update, churn, $CACAO donation, and trading activation. Keep yourself updated by following our [Discord](https://discord.gg/mayaprotocol) & [Twitter](https://twitter.com/Maya\_Protocol).
* We're developing a Community Ambassador initiative aimed at expanding our presence in untapped markets and communities, including Dash and Kuji.
* We delayed the donation event due to incomplete and error-filled memos. The issues were resolved promptly, but we urge our community to handle funds with care and responsibility.
* Genesis Nodes protect with trust, Liquidity Nodes with incentives. Liquidity Nodes can boost their capital efficiency by earning both Validator and Liquidity Provider rewards.
* Your ideas and feedback are invaluable to us. To contribute to Maya, review our Gitbook, or if you are a senior cosmos developer, contact us. More eyes on the code mean better outcomes.
* Check the complete Maya [episode](https://twitter.com/i/spaces/1OyJAVNvZabxb?s=20), and stay updated on our latest news by joining our [Discord](https://discord.gg/mayaprotocol).

</details>

**#12:** [https://twitter.com/Maya\_Protocol/status/1642894658803232769](https://twitter.com/Maya\_Protocol/status/1642894658803232769)

<details>

<summary>Key points</summary>

* Maya Protocol raised $11,421,176.19 and will start operations with approximately $23 million TVL.
* We're preparing for the $CACAO Donation and will enable trading as soon as possible with minimal slippage.
* %81 of Liquidity was allocated to Tier 1 which exceeded expectations.
* Having unique chain integrations, such as Dash & Kuji, is a top priority to have attractive swap routes.
* 20k $MAYA has been airdropped and THORWalletDEX is working to make them visible with correct balances on both iOS (testflight) and the web interface.
* Maya Protocol is working on a non-reflexive pool during the summer. Stay tuned for more details.

</details>

**#11:** [https://twitter.com/Maya\_Protocol/status/1640368305969016833](https://twitter.com/Maya\_Protocol/status/1640368305969016833)

<details>

<summary>Key points</summary>

* We have raised over $6 million with 3 days left to go! Maya Protocol is now officially viable, and we will be launching $CACAO on the 31 of March.
* If you're Cosmos SDK savvy and want to help us to grow the ecosystem, reach out! We are looking for more hands and ideas.
* A few days ago, Thorwallet figured out a way to add liquidity through Xdefi with Ledger.&#x20;
* We are polishing the last details for the first batch of Maya Rewards for Tier 1 liquidity providers, so stay tuned as the airdrops are about to begin.
* El Dorado intends to join Maya's ecosystem by introducing a swap platform to provide users with more options and increase exposure. The platform will go live following the $CACAO event and activation of Trading.

</details>

**#10:** [https://twitter.com/Maya\_Protocol/status/1637869579471839235](https://twitter.com/Maya\_Protocol/status/1637869579471839235)

<details>

<summary>Key points</summary>

* After opening up the $Rune pool, we went from 800,000 to 4.2m 48 hours later!
* 93% of the liquidity raised this far is Tier 1, meaning that for 200 days, $CACAO will have a stable price at launch.
* What's next? 10M, this is an important number because after we open up the savers and with a fifty percent sync cap 30 to 40 dollar mark on TVL would be already 1/3 of the Thorchain pool.
* After that, our goal is 20M. Pools would be competitive; why is this number so important? We will be taken more seriously, and other interfaces and wallets can't help to notice us.

</details>

**#9:** [https://twitter.com/Maya\_Protocol/status/1635355237258248192](https://twitter.com/Maya\_Protocol/status/1635355237258248192)&#x20;

<details>

<summary>Key points</summary>

* Tested churning with our own nodes in another Stagenet. Everything worked perfectly. On March 13th at 4:00 p.m. CST we had a smooth and successful churning event of our genesis nodes to Stagenet.
* We will churn Genesis nodes to the Mainnet today and complete a system check. You can follow our progress more closely on the #genesis-nodes channel on discord [https://bit.ly/3LnnPQx](https://t.co/44rvV88wzO)
* The extra $MAYA token rewards will be happening from Wednesday at 10 a.m. CST to Sunday at 10 a.m.

</details>

**#8:** [https://twitter.com/Maya\_Protocol/status/1632908951804157952](https://twitter.com/Maya\_Protocol/status/1632908951804157952)&#x20;

<details>

<summary>Key points</summary>

* Minting the first block, having all genesis nodes churn in, and opening up THORWallet and inbound addresses publicly for all to add funds will not happen simultaneously.

<!---->

*   Here's a more realistic timeline:

    \-Tuesday (07/03): Mainnet launched, and the first block minted.

    \-Wednesday (08/03): Sanity checks and internal testing.

    \-Thursday (09/03): Churn once the last Genesis Node reaches the Ethereum Network block tip.

    \-Friday (10/03): Significant liquidity can be added if everything looks good.
* MAYA allocation model is changing to be time-dependent instead of being dependent on the first inflows to pools. The first 75% of the award per pool will be given to people adding T1 liquidity from Friday, March 10th to Tuesday, 14th.

</details>

**#7:** [https://twitter.com/Maya\_Protocol/status/1630236344772448256](https://twitter.com/Maya\_Protocol/status/1630236344772448256)

**#6: Maya & Thorwallet** [https://twitter.com/Maya\_Protocol/status/1627700335836790787](https://twitter.com/Maya\_Protocol/status/1627700335836790787)

<details>

<summary>Key points</summary>

* THORWallet is currently in the last steps of testing. The Liquidity Auction is completely integrated into the mobile app and currently working on the previous stages of the web app integration.
* Halborn is currently working on the DASH bifröst audit. Our team prioritized this, and the final report will be available soon. Next on our list is [$AZTEC](https://twitter.com/search?q=%24AZTEC\&src=cashtag\_click), so stay tuned for more updates.
* **NEW:** Maya's cash-back program. Provide liquidity through a wallet and receive a link to share with friends. If they use your link to provide liquidity, you earn a percentage of their liquidity.

</details>

**#5: Maya & Halborn** [https://twitter.com/Maya\_Protocol/status/1625132975607418880](https://twitter.com/Maya\_Protocol/status/1625132975607418880)

<details>

<summary>Key points</summary>

* Halborn is giving Maya an audit of the code and a 360º analysis of new security measures in the market and economic security assessments.
* &#x20;Once Maya’s code audits are done, Aztec will be the next in line.&#x20;
* Maya’s first stage was to learn and research about THORChan: how and why it was implemented that way and how they Maya could manage to cover the same issues, and more. The actual stage consists in implementing.&#x20;

</details>

**#4:** [https://twitter.com/Maya\_Protocol/status/1622746989724110848](https://twitter.com/Maya\_Protocol/status/1622746989724110848)&#x20;

<details>

<summary>Key points</summary>

* The team is working on updating Maya to the TC version that supports savers, V101, and savers will be available at launch.
* Stage testing will be started when V101 is ready, and a stage liquidity auction will be conducted around Feb 20th.
* The THORWallet app has the tiers part of the liquidity auction integrated, and an affiliate program will be launched with THORWallet, rewarding $Maya tokens.
* There are plans to reward the first Tier 1 liquidity providers under 5M USD with 1% of $MAYA tokens to incentivize joining the LA early.
* Any wallet with a custom memo function can send funds to the LA, and every integrated chain affects the difficulty of integration.

</details>

**#3: Maya & Thorstarter** [https://twitter.com/Maya\_Protocol/status/1620210390641659912](https://twitter.com/Maya\_Protocol/status/1620210390641659912)&#x20;

<details>

<summary>Key points </summary>

* Thorwallet is looking for ways to incentivize people to participate in the Liquidity Auction, and more details will be shared later.
* Halborn Security has performed six security audits for Maya, and they are waiting for the final report to end the process.
* Aztec is coming soon, and CosmWasm and IBC will be enabled from the start for powerful composability.

</details>

**#2:** [https://twitter.com/Maya\_Protocol/status/1617673467599687680](https://twitter.com/Maya\_Protocol/status/1617673467599687680)

<details>

<summary>Key points </summary>

* Liquidity nodes have been improved by bonding LP units with profound intentions, resulting in higher yield, more people adding liquidity, more pool depth, and more affordability.
* Genesis nodes are motivated by a desire to protect users and are open to more than six nodes willing to put their reputations on the line.
* Economic incentives for nodes will be significant from the start, and genesis nodes will eventually be churned to make the system more secure and sustainable.

</details>

**#1:** [https://twitter.com/i/spaces/1dRKZMqNNqQxB?s=20](https://twitter.com/i/spaces/1dRKZMqNNqQxB?s=20)

<details>

<summary>Key points</summary>

* The latest Halborn Audits report shows no major vulnerabilities in Rune & Maya pools. Full report will be shared with the community in the following weeks.
* Supported tokens on the liquidity auction: $BTC, $RUNE, $ETH, and some stablecoins, with plans to add $KUJI, $Osmos, $BSC, and $BNB after the auction to reduce risks and increase security.
* The first three Maya Nodes will be run by @THORWalletDEX, @Thorstarter, and @Maya\_protocol, with an opportunity for organizations with a stake worth value to join the genesis nodes.

</details>

## Twitter Spaces with other hosts

**Cosmos Club & Aaluxx:** [https://twitter.com/CosmosClub\_/status/1622881702233141249](https://twitter.com/CosmosClub\_/status/1622881702233141249)&#x20;

**LPU & Maya:** [https://twitter.com/i/spaces/1OwxWwqXXaVxQ](https://twitter.com/i/spaces/1OwxWwqXXaVxQ)&#x20;

**AMA with Danku:** [https://twitter.com/Maya\_Protocol/status/1616118122137657345](https://twitter.com/Maya\_Protocol/status/1616118122137657345)

**THORWallet x Maya integration:** [https://twitter.com/THORWalletDEX/status/1597528829207392256](https://twitter.com/THORWalletDEX/status/1597528829207392256)&#x20;

**Liquidity Auction Tier ROI calculator with Kyle Krason:** [https://twitter.com/Maya\_Protocol/status/1630723295254548482](https://twitter.com/Maya\_Protocol/status/1630723295254548482) [\
](https://twitter.com/NACSTWEETS)
