# Philosophical perspective RO

If decentralized Cross-Chain technologies replace CeFi, they will require serious liquidity depth and tremendous volume. And because volume follows depth, this should be our main focus and priority, only behind security. With enough depth, slip fees make swaps more affordable for larger trades too.&#x20;

Our Stable Pools feature one additional step to increase our liquidity and depth - should Maya 3.0 be enabled —plus, as we outlined in [Part 5](broken-reference), the creation of $USc tokens and their subsequent lockup inside pools is a net positive for our ecosystem and for the economics of $CACAO.&#x20;

Our offering of pools that imply less uncertainty is also obviously great news for institutional investors and more conservative players since they can get exposure to our LP opportunities and to our protocol success without having to worry too much about different sources of volatility.&#x20;

Because of the way orders are routed using both regular and Stable Pools - see details in our Economic Overview - when trading assets across different blockchains, users experience a bigger notional depth, one that sums the depth of all the pools involved in their trade, with the corresponding slippage and slip fee benefits that this brings.&#x20;

Outside of the Maya blockchain, as usual, only one transaction needs to be sent, and only one transaction needs to be received, so gas fees in the source and target chains are not increased.&#x20;

**Notice how this is all positive for the Maya Economy in many ways:**

1. More perceived depth means lower slip fees, and with the increased affordability comes increased swap demand and, thus, the better yield for the system players.
2. To add $USc to Maya Stable Pools, an investor had to buy $USc (generating demand) and lock it all up in the LP position. Although impermanent loss is greatly reduced in Stable Pools, Impermanent Loss Protection is still attractive and serves as an additional incentive for the Stable LP Units to remain for at least 100 days.
3. For the investor to have acquired $USc, the investor had to buy it from someone who had to post $USa, $USm, $USs, and $USb as collateral at the voted portfolio distribution. In the process, $CACAO expatriation fees were paid, $CACAO was burnt, Synths were minted (increasing pool depth in Maya), and short-term debt was converted to long-term debt.
4. $USc is brought to Maya and paired to its primary market and generates yield there, creating real and deep-rooted demand.
5. Stable Pool’s LP units won’t count towards bondable liquidity for our nodes. This means that the system would tend toward the lower end of our Incentive Curve for Liquidity Nodes (see Chapter 3) and would increase yield for our nodes, making it more attractive for LPs to upgrade and compete in the churn rounds while generating $CACAO demand and increasing the security budget.&#x20;

Finally, to any and all User Interfaces that already work with THORChain and that wish to connect into Maya to increase reliability and decentralization, we suggest that they use this method too, breaking big transactions into smaller ones and routing them through both protocols, proportional to the best available pool depths. This would, of course, result in better trading fees while benefiting both LPs in both protocols. In the following Economic Overview section, we explain why it is economically attractive for User Interfaces to operate in this way.
