# Economic overview RO

Inside Maya, all trades to or from $CACAO, as well as trades to or from $USc, can clearly use only one pool; for example, someone trading BTC for $CACAO will readily use the BTC / $CACAO pool. However, for “longer” swaps, like BTC to ETH, the transaction can be completed using all four pools, as long as the partition is known for how to get lower slip-based fees. In the same example, these four pools are BTC / $CACAO, BTC / $USc, $CACAO / ETH, and $USc / ETH.&#x20;

The optimal amounts and paths considering four pools is not trivial, given that they can all have different depths and that you need to commit to a proportion of the funds from the beginning.

<figure><img src="../../.gitbook/assets/WHITE PAPER  (1).png" alt=""><figcaption></figcaption></figure>

To find the best proportion to transact into the pools we need to get the local maximum of a function that takes into account the differences in depths and the truncation of the routes. Note: for this derivation, it is advised to already understand the CLP derivation THORChain created in their design of the Slip Based Fees for AMM swaps:&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -15.png" alt=""><figcaption></figcaption></figure>

Where: X is the balance of the inbound asset, Y is the balance of the outbound asset, x is the inbound asset input and y is the outbound asset output to the desired wallet. The difference between y and x\*Y/X is the slip fee, which is retained in the pool.&#x20;

This equation depends on knowing the balance of each asset before the transaction, and we can further simplify it by using percentages instead of absolute numbers. If x is input as a percentage of X, then both X and Y cancel out (as 100% = 1), and y becomes also a percentage output. The simplified equation looks as follows: &#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -16.png" alt=""><figcaption></figcaption></figure>

Translating the equation into percentages also helps abstract away the underlying asset balances and with this simplified version, we can truncate it into itself one more time to describe a single route (by substituting y with the equation above):&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -17 (1).png" alt=""><figcaption></figcaption></figure>

The above represents the equation of one route through two consecutive pools that charge slip-based fees. This equation assumes both pools are equally deep so, to adjust for the depth difference, we introduce the first weighting factor Dn , which is the measure of how much deeper (or shallower) the second pool is relative the first pool along route n, calculated simply as the depth of the second pool divided by the depth of the first pool in terms of the common asset they share. Notice Dn > 0.&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -18.png" alt=""><figcaption></figcaption></figure>

We have added the subscript “1” to z and x as well, to label the route as Route 1. Now that we have the generalized equation for one route, let’s sum two together, simply like so:&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -19.png" alt=""><figcaption></figcaption></figure>

Where: total z is the sum of both route’s assets. This sum assumes both routes are identical and is not breaking up the asset allocation x1 and x2 as a zero-sum game. To set these equations we need to have x1 depend on x2 and vice versa. To continue we will require a few significant adjustments:&#x20;

First, we will stop using x1 and x2 as the percentages of the inbound asset. Instead, we will substitute them with new variables:&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -20.png" alt=""><figcaption></figcaption></figure>

Where Ain is the inbound asset traded into Maya as a percentage of the sum of both inbound pools’ inbound assets, p is the proportion of the inbound asset to be swapped through Route 1 and (1-p) is the proportion of the inbound asset to be swapped through Route 2. R1 is the Relative Depth between the 1st Pools along each route, calculated simply as the depth of the first pool of Route 1 divided by the depth of the first pool of Route 2, in terms of the common asset they share. Finally, z1 and z2 must be weighted as well, so we similarly introduce R2 to weigh the Relative Depth between the 2nd Pools along each route.&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -21.png" alt=""><figcaption></figcaption></figure>

**Substituting everything and simplifying, we get:**&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -22.png" alt=""><figcaption></figcaption></figure>

R2 has been substituted to constrain it to its possible values along the determined line defined by the other weights. To avoid confusion, Aout has been introduced to substitute zt , defined as the outbound asset traded out of Maya as a percentage of the sum of both outbound pools’ outbound assets. Notice that by definition: 0 < Ain , Aout , p < 1, as well as that all weights are greater than zero.&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -23.png" alt=""><figcaption></figcaption></figure>

The diagram above shows each weight more clearly. All weights can be easily calculated given that they always share a common asset to compare depths - “apples to apples”. “A” stands for Asset, and “I” stands for intermediary.&#x20;

Wherever the first order derivative of Aout with respect to p is equal to zero between 0 < p < 1 maximizes Aout, essentially the optimal proportion p to break the inbound transaction through the first route and sending 1-p through the second route, minimizing slip fees for the transaction.&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -24.png" alt=""><figcaption></figcaption></figure>

This is also the optimal strategy for incumbent and future User Interfaces, Wallets and Institutional Investors routing large transactions (where slip is most important). As long as the slip savings are larger than the double gas expense on inbound and outbound chains, it is in their best interest economically to break the transaction up routing volume to both Maya and THORChain. User Interfaces can implement the above optimizations using THORChain as Route #1 and Maya as Route #2. So, everytime the following condition is met, User Interfaces would be better breaking up the transaction:&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPERr -25.png" alt=""><figcaption></figcaption></figure>

Where MOD stands for Maya Outbound Depth and TOD stands for Thorchain Outbound Depth. Assuming Thorchain is Route 1 and Maya is Route 2.&#x20;

This is key, given that it is not only more convenient for User Interfaces to have more than one provider for reliability and redundancy matters, but there are also direct and economical incentives to integrate and use various Thorlikes simultaneously, for each transaction.&#x20;

This in turn increases the likelihood of a multi-thorlike future of cross-chain technologies, where a network of L0’s collaborate together, route liquidity to each other and mutually increase their resilience.&#x20;

The optimization works for any arbitrary number of routes (by adding more terms, with their appropriate weights), so it would work for any number of interconnected cross-chain liquidity networks. Keep in mind that servicing inbound transactions is throttled down as transaction size grows bigger, so this strategy would also yield faster swaps given their smaller size relative to each protocol’s depth when broken down.&#x20;

Interestingly, our simulation results have been outstanding. Even if Thorchain pools are 3x deeper than Maya, users still get around 23.8% saved in transactions of 1% depth of the inbound pools when using the optimized double route. That is very significant for any user at those amounts, so User Interfaces should really take supporting Maya seriously. It is important to note that User Interfaces would still get the same affiliate fee in absolute amounts, their users would just pay less, making User Interfaces who support Maya more competitive than those who don’t. To illustrate an example, BTC pool in Thorchain at the time of writing is $35M and ETH pool is $20M. If Thorchain is 3x deeper, it would mean Maya has $11.67M and $6.67M in its BTC and ETH pools respectively. A 1% transaction of the inbound asset would mean $233,000 being swapped from BTC to ETH, 24% saved by going optimized double route would mean $3,800 USD saved by the swapper, no loss for the User Interface and both Thorchain and Maya being more attractive overall, jointly increasing cross chain demand thanks to increased affordability. This is how we get to mass adoption.&#x20;

For a 2% transaction of the total inbound asset (Ain ), with all else equal, the savings is “lower” as a percentage of total fees paid but significantly higher in absolute terms. The swapper would save 22.7% on $16,000 in fees paid: $13,800 in savings going Double Route.&#x20;

On the other end of the spectrum, for a 0.2% transaction of total inbound assets, 24.7% is saved on $675 in fees paid: $167 in savings going Double Route. At these levels, gas fees in the source and especially the target chain start playing a more significant role with current depths, but as TVL grows larger in both protocols, savings in absolute terms increase while gas fees remain the same, making it ever more attractive to go Double Route at lower slip transactions.&#x20;

It is important to note that for Maya to have $11.67M and $6.67M depth in its BTC and ETH pools, it just needs to raise half of those amounts during the Liquidity Auction, given that the CACAO side is being donated. $5.83M of BTC and $3.33M of ETH are very much attainable given the scope of Maya, its very attractive launch model, and its battle-tested code.&#x20;

The same goes for $USc pools in the future, if 25% of liquidity in Maya is in $USc paired pools, our users will perceive 24.7% savings in 0.02% inbound asset transactions that are routed to Maya, and this time gas fees don’t play a role given that there is only 1 inbound transaction in the source chain and 1 outbound transaction in the target chain.&#x20;

As a last economic remark, external assets in Stable Pools in Maya do not count directly towards the deterministic value of CACAO, given that it is not paired with CACAO. Despite this, more funds in stable pools create lower slip fees and, therefore, larger swap volume without increasing bonded liquidity, therefore increasing yield for Nodes who need to buy CACAO using external assets to then add CACAO + ASSET liquidity, increasing TVL in the process.&#x20;

This is because liquidity provided into our “Stable Pools” by any node will never count towards its bond. In other words, the LP tokens that help them churn in can only come from bonding with $CACAO to help it stay as our main trading token and as the supermajority of our liquidity. Remember also that, as we covered in Chapter 5, $USc has a capped supply amount, which comes from the limits of the underlying stablecoins that comprise it, to prevent over-leverage in the system. Between these two phenomena, the Stable Pool share of total liquidity will always remain below 33% but realistically will be as low as 10% to 15%. Above 33% Stable Pool share, Liquidity Rewards are 0, and all block rewards go to Nodes equally, providing no incentive for LPs or Nodes to add more $USc paired liquidity and even remove it while incentivizing new Nodes to pair $CACAO in pools to churn-in. In other words, adding more liquidity to Stable Pools has quickly diminishing returns for all players adding Stable Pool Liquidity as its share gets closer to a 33% share, a desired behavior.
