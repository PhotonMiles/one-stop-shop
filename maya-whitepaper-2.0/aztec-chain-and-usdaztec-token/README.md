---
description: A new ecosystem for $CACAO
---

# 🔴 Aztec Chain &  $AZTEC token

Aztec is a powerful demonstration of the high potential of the Maya Protocol’s design and how it can expand horizontally to offer different complementary products. We are excited about the endless possibilities of combining Maya’s liquidity black hole properties with Aztec’s Smart Contracts and economic capabilities!

### ELI5

1\. Smart Contracts are necessary to add complexity and dynamism to flourishing crypto economies because they provide builders and developers with the flexibility to create and experiment. Smart Contracts will provide an avenue for Maya derivatives —i.e., Synths— to find use cases.

2\. Aztec is a fork of Cosmos. We decided to do this based on several factors like mature infrastructure and extensive Smart Contract development.&#x20;

3\. Aztec will take a role in the algorithmic stablecoin quest, but it will not have a “one mechanism for all” approach. Instead, it will work with a full suite of different tokens that coexist, complement each other and offer different types of risks. Our algorithmic stablecoins will be turned off at launch, giving us time to finish with the economic design and to run the appropriate bounties’ programs, etc. Maya and Aztec will never subsidize yield to inflate demand for any of their own stablecoins or derivatives.&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -03.png" alt=""><figcaption></figcaption></figure>

\
