# Philosophical perspective AC

Aztec’s mission is to complement and underpin the DeFi space. With this project, we express our strong belief in permissionless money and in truly open and transparent monetary platforms controlled exclusively by open-sourced coded rules and policies.&#x20;

Although we could try to achieve this with Linklikes, Osmolikes, or Etherlikes side chains, we believe Cosmos is an excellent first step, especially given the high complementarity —and stark difference—  with Maya, our existing Thorlike chain.&#x20;

The Aztec protocol also comes in at a big and important moment for our industry after the Terra / LUNA collapse. Yes, we will be seeking to contribute to the decentralized stablecoins experimentation and to continue the iteration progress without losing any sight of the successes and mistakes of past protocols. Please refer to [Roadmap: Maya 3.0 ](broken-reference)section for more details on our upcoming stablecoins.&#x20;

Finally, the more useful we can make our token, $CACAO, the stronger the Maya ecosystem becomes. We believe the Aztec Chain serves this purpose adequately.
