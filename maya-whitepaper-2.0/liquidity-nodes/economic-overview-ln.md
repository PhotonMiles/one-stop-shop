# Economic overview LN

Liquidity Nodes result in adjustments to the Pure Bond Model security policies, which we tailored to accomplish three important things:

1\. More than 75% of capital should be bonded by our nodes to keep them honest. In fact, closer to 87% is preferred. A Sybil attack at 75% nets a breakeven, and with any higher bond percentage it nets a loss.&#x20;

2\. This capital balance must be found by rational market forces (i.e., supply and demand).&#x20;

3\. We must incentivize decentralization (i.e., a high node count).&#x20;

\*The magnitude of these losses assumes the $CACAO price did not rise as the attacker accumulated $CACAO, added it as LP in different addresses in one block, and churned in 67% of the Nodes on the same block. This is an unreasonable assumption since the buying pressure of the accumulation would bring $CACAO’s price higher, making the cost basis of the attacker higher relative to incumbent Nodes and LPs, making his/her loss higher. Additionally, adding more LP positions increased TVL, again increasing the $CACAO price to the attacker at a profit to incumbents. Finally, churning in is limited and successfully doing so with many Nodes while competing against other Nodes is difficult, given the attack so far has made optics for Maya bullish and become a Node more attractive. If there are 40 Nodes, the attacker needs to churn in 27, winning the Liquidity Bond war at least 27 times (and as more of the Attacker’s nodes are churned in, it is more likely at churn-out that one of its nodes is churned out). This all amounts to a significantly higher loss to the Attacker than the aforementioned breakeven.&#x20;

We call our resulting model “The Incentive Curve,” and it works by algorithmically balancing the nodes’ and markets’ incentives to either provide more liquidity or bigger bonds by increasing or decreasing the participants’ rewards on each of these sides periodically.

### **The Incentive Curve:**&#x20;

**For every new block…**

<figure><img src="../../.gitbook/assets/WHITE PAPER ecuacion.jpeg" alt=""><figcaption></figcaption></figure>

For any individual LP, Earnings is equal to the increase in value of their own LP Units due to the Yield that was kept in pools that LP participates in.&#x20;

For any individual Node, Earnings is equal to the increase in value of their own LP  Units due to the Yield that was kept in pools that LP participates in, plus the Node Exclusive Reward divided by the number of Nodes.&#x20;

**Please note:**&#x20;

* Nodes earn both Node Exclusive Rewards (NER) and Liquidity  Rewards.&#x20;
* LPs earn only Liquidity Rewards (LR).&#x20;
* Node Exclusive Rewards (NER) is distributed evenly among all nodes whereas Liquidity Provider Rewards (LR) is paid out relative to their bonded liquidity.&#x20;

All the calibrations of the economics and incentives that manage the system are algorithmic and code-driven. Whenever the total network’s liquidity is tilted too much into either side of the spectrum (too much-bonded liquidity vs. too much-provided liquidity), the incentive mechanism reacts by balancing out the rewards conversely. Visually, the curve looks like so:&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -43.png" alt=""><figcaption></figcaption></figure>

And it basically creates a liquidity flywheel where:&#x20;

* At a moderately highly bonded state, there is an incentive for new capital to be added via LP’ing. Liquidity can be added this way much more rapidly than nodes’ can churn in or out. &#x20;
* If too much liquidity is provided by LPs, then the network would tip into an unsafe state, which would incentivize the nodes to bond more capital, benefiting our depth and volume in the process again.
* Liquidity is bonded by the nodes that bring the most liquidity on the next churn, and the system returns to or above the balance.
* Notice that TVL is increased when the incentive mechanism pulls in either direction, whereas on THORChain the pull increases TVL in only one direction. This means assets are pulled into the protocol regardless of whether it’s moving from unsafe towards over-bonded or it’s moving back.&#x20;
* This process can go on and on, attracting new liquidity every time, as long as cross-chain swaps remain plentiful in a bright multichain future.&#x20;

We can compare the behavior of this model with respect to the traditional one by normalizing through “Node Premium,” where Node Premium is a measurement of how much more a node earns with respect to LPs per unit of capital invested, on average. On both Thorchain and Maya, the designed equilibrium is at Node Premium = 2, where nodes earn, on average, twice as much as LPs. This point sits at 66% bonded native tokens in the Pure Bond Model and at 87% bonded liquidity in Maya.

<figure><img src="../../.gitbook/assets/WHITE PAPER dark (1).png" alt=""><figcaption></figcaption></figure>

The following table shows this comparison, i.e., how much more a Node earns than LPs for the same investment in those conditions, assuming the same fees and tradeable liquidity. “Node M/T” refers to the increase in earnings for equivalent swap volume conditions between our Liquidity Node’s model and the Pure Bond Model. Same for “LP M/T” for Liquidity Providers. It is important to mention that the Maya figures in this comparative analysis already consider the 10% $MAYA Token deduction. We are comparing apples to apples here. Finally, M/T comparisons were done by multiplying a one-day ROI by 365. APY would be compounded and show a greater difference.

<figure><img src="../../.gitbook/assets/tablas -35.png" alt=""><figcaption></figcaption></figure>

<figure><img src="../../.gitbook/assets/tablas -36.png" alt=""><figcaption></figcaption></figure>

<figure><img src="../../.gitbook/assets/tablas -38.png" alt=""><figcaption></figcaption></figure>

<figure><img src="../../.gitbook/assets/WHITE PAPER tabla.png" alt=""><figcaption></figcaption></figure>

Notice a few things from the table above. The first is we are conserving the Pure Bond Model security boundaries at the same Node Premiums, making security relatively equivalent. A small caveat here is that losses are steeper with the Pure Bond Model than they are on Maya due to the attacking node losing all its native tokens while on Maya, $CACAO is only half of the losses at stake to the Attacker. So although breakeven for attacks is at the same Node Premiums, losses rise more quickly from above that point on the traditional model. &#x20;

This consideration is important since although a rational actor would not consider attacking at a 20%+ loss\*, an irrational/externally motivated bad actor could accept the loss. This is why it is important to have a diversity of chains with deep liquidity, making it more difficult and expensive for irrational actors to take down the entire cross-chain DEX infrastructure. The bigger the TVL, the bigger this loss is in absolute terms and the harder it is for irrational actors to risk enough funds for an attack. &#x20;

The second thing to notice from the table above is that the increased yield relative to the legacy incentive pendulum model is higher at higher bonded states. This means that Maya operates much better at high bonded states when compared to the old model. This is good since we generally prefer to err towards the side of over-bonding. As the tip scales towards the under-bonded state, the system stops becoming noticeably better than the old model. &#x20;

Finally, notice Nodes never risk earning fewer returns per dollar invested than LPs do. By the time earning parity is marginally reached (Node Premium = 1.01),  although LPs earn great returns on the investment and it is very attractive to become an LP, Nodes still earn more than LPs and then the same at the limit. This means that it is much more likely that more LPs join when reaching these over-bonding levels than Nodes leaving, given they are still getting an attractive return on their investment. &#x20;

### On slashing…&#x20;

Slashing mechanisms needed a little revamp too, since whenever we slash a node, we are still interested in keeping their liquidity available in our pools. Additionally,  sometimes slashing is a mistake, so we need to account for these slash points but only execute them once a Node withdraws its liquidity. We are thus introducing  “anti-LP units,” which are assigned to nodes that showed potential malicious behavior or downtime in proportion to their merited slash. These Anti LP units specify the value accrual of a slash point’s liquidity that no longer belongs to the penalized nodes and how much of their assets will be redirected to the Protocol  Owned Liquidity whenever these nodes decide to withdraw their bond.&#x20;

When any node’s Anti LP tokens become 20% of their provided liquidity, it becomes dangerous that they protect any funds since they no longer own a  significant part of their original bond. These nodes are, therefore, subject to being banished, which means they are unbonded, and their assets are completely redirected to paying back all owed liquidity to the protocol through the slash fees generated.

Nodes can avoid being banished —also losing ILP seniority and their node spot—  by adding more liquidity to offset this Anti LP tokens percentage and then waiting to be churned out to settle any pending accounts while they are unbonded.&#x20;

Manual and automatic forgiving of slashing work using these Anti LP units too,  with mechanisms designed to remove them in special situations like whenever all the nodes accrue them simultaneously or because of any critical consensus failures.&#x20;

**Liquidity Nodes in a Nutshell**&#x20;

1. Capital Efficiency is no longer inversely proportional to Network Security.&#x20;
2. All of TVL is in pools and is actively traded, making Maya significantly more productive with capital.
3. Increased Capital Efficiency means increased average yield for all ecosystem players.
4. On average, Nodes with lower bonds get higher returns per dollar invested than Nodes with higher bonds, making churn-in competitions fiercer and contributing to decentralization and bond homogenization.&#x20;
5. As more Nodes compete to churn in, they add more liquidity. This increased liquidity turns the Incentive Curve further down making it even more attractive to become the winner Node. &#x20;
6. As Bond Wars compete on Liquidity Provider Units, Pool Depth increases.
7. As the incentive curve system pulls in any direction, Pool Depth increases.&#x20;
8. &#x20;Increased Yield and increased Pool Depth make affordable swaps more likely.&#x20;
9. Node to LP and LP to Node latency is reduced and very easy to do for  Operators without incurring slip fees.
10. Standby Nodes earn yield while they wait to win the Bond War and churn in, making it less risky to compete.&#x20;
11. Nodes no longer need 100% exposure to $CACAO, making it more likely for Institutional Investors to opt-in as Nodes.&#x20;
12. Node misbehavior causes the slashing of a Node’s LP units that are converted into Protocol Owned LP Units that count towards unbonded liquidity. These Protocol Owned LP units will never exit, staying as a buyer of last resort.
13. Liquidity Auction makes a lot of sense in Maya due to all capital already being locked in Pools as LP ahead of Liquidity Node churn-in competition.&#x20;
14. Liquidity Auction is the cheapest time for a Node to acquire enough LP  units to compete for churn, making it very attractive for aspiring Nodes to participate in the Auction with as much liquidity as they feel comfortable with, setting Maya up for deep pools from the very beginning.

**Impermanent Loss Protection (ILP) in Maya.**

ILP guarantees that LPs will either make a profit or at least recoup their investment in comparison to holding them externally after a certain time period (currently set to 50 days after deposit) and even that they can be partially reimbursed for any impermanent losses incurred before that time. Any potential losses in the LP deposit are subsidized in $CACAO.

To fund this feature, there is an ILP reserve that will receive 10% of all the originally minted $CACAO tokens (10,000,000), and that will be replenished recurrently with 10% of all the system income.

Impermanent Loss Protection can be expensive, especially with a token meant to be non-inflationary. Currently, impermanent loss protection is paid out in the following 4 cases:

1\. ASSET increases in price relative to CACAO,

2\. CACAO decreases in price relative to ASSET,

3\. ASSET decreases in price relative to CACAO,

4\. CACAO increases in price relative to ASSET.

We are now introducing Asymmetrical Impermanent Loss Protection, where cases 1 and 2 are protected with the typical 100-day period, but cases 3 and 4 are protected with a different period, namely 400 days. Why? Because even without any ILP, cases 3 and 4 were already better off, thanks to CACAO being a stronger asset through this subtle feature change.

Let’s take a closer look. To better understand what will be detailed here, it is recommended to read this [article](https://crypto-university.medium.com/impermanent-loss-and-impermanent-loss-protection-a4a0f78d1701).

**Example**

There is a CACAO/ATOM pool. Let us assume that the pool provided an 8% return in 100 days. A Liquidity Provider invests $10,000 in total ($5k in ATOM and $5k in CACAO) and then decides to withdraw after exactly 100 days. In this example, we will assume NO Impermanent Loss Protection for cases 3 and 4, although it will just be more limited in practice, not completely deleted.

Case 1. ATOM went up 200%

Case 2. CACAO went down 75%

Case 3. ATOM went down 75%

Case 4. CACAO went up 200%

In all cases, K = 13.4% loss, since there is 3x ratio change. We will compare against

1\. HODL Value,&#x20;

2\. Invest only $5,000 on ATOM and&#x20;

3\. LP value.

**Case 1.**

HODL Value is $15,000 in ATOM and $5,000 in CACAO = $20,000

Only in ATOM = $30,000

LP Value is $20,000 \* (1 - 13.4%) \* 1.08 = $18,705.6

In this case, the LP is withdrawing from a pool where the asset provided most definitely has provided value to the network but CACAO’s price stayed stagnant given the rest of the pools in the network. In other words, ATOM has during that time provided strength to CACAO and the investor would have been better off just investing in ATOM. The investor most definitely deserves ILP and to be given back at least the $1,295 to make up for the LP vs. HODL metric.

**Case 2.**

HODL Value = $5,000 in ATOM and $1,250 in CACAO = $6,250

Only in ATOM = $10,000

LP value is $6,250 \* (1 - 13.4%) \* 1.08 = $5,845.5

In this case, the LP is withdrawing from a pool where the token price remained stagnant, at least providing stability to the network, but the rest of the network plummeted, making CACAO worth significantly less. The investor was severely harmed by their investment In other words, ATOM has, during that time provided strength to CACAO, and the investor would have been better off just investing in ATOM. The investor most definitely deserves ILP and to be given back at least the $405 to make up for the LP vs. HODL metric.

**Case 3.**

HODL Value = $1,250 in ATOM and $5,000 in CACAO = $6,250

All in on ATOM = $2,500

LP value is $6,250 \* (1 - 13.4%) \* 1.08 = $5,845.5

In this case, the LP is much better off thanks to having been invested in CACAO and LP’ing instead of full exposure to ATOM. Now that ATOM has collapsed in value, the investor is attempting to exit now instead of staying to socialise the losses incurred despite having had great benefit from a stable CACAO value.That is, the investor would have kept the up side but is making the rest of the network share in the losses. We believe this investor should only be given back the incurred $405 loss in a much longer time horizon, if at all.

**Case 4.**

HODL Value = $5,000 in ATOM and $15,000 in CACAO = $20,000

All in on ATOM = $10,000

LP Value is $20,000 \* (1 - 13.4%) \* 1.08 = $18,705.6

In this case, the LP has had a 87% return already on the $10,000 investment not thanks to the returns of the pool but to the strength and growth of CACAO. Now, even when the asset provided underperformed greatly, the investor seeks an additional 12.95% return to make up for LP vs. HODL from the Network’s reserves, even when the network itself provided the growth to CACAO that this investor enjoyed. We don’t believe this investor should have made up this additional $1,295, at least in a short time horizon.

As you can see, the investor in Cases 1 and 2 decides to leave the network better off, thanks to the investment during that period. In contrast, in Cases 3 and 4, the investor decides to leave the network worse off or benefit greatly from the rest of the network is much better off instead of staying with the asset until it is better off and realizing the earnings or losses then. In other words, cases 3 and 4 attempt to socialize the losses or stagnation to the rest of the network. For this reason, we look to decide to increase the time horizon of the ILP for cases 3 and 4, so it is at least given to liquidity providers who are committing their funds to the network for longer and are not doing opportunistic short-term flipping.

With this small change, Maya saves around 37.5% of ILP paid out on average; even more when ASSETS underperform or CACAO overperforms during heavy withdrawals (the mix of Cases 3 and 4 increases vs. Cases 1 and 2). When full impermanent loss is paid out to cases 3 and 4, it is because of strong, deeply rooted 400+ day liquidity positions. The network still protects investors who chose great assets to contribute or were harmed by the underperformance of $CACAO more readily. Overall we believe this is a more scalable design. So where does CACAO for ILP come from?&#x20;

* 10,000,000 CACAO will go directly to the reserve for this purpose from the beginning.
* 10% of the system income of all swaps and transactions will replenish the reserve.



