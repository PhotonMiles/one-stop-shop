# Philosophical perspective LN

There are several economic and tokenomic implications when Node operators become Liquidity Providers simultaneously:&#x20;

The efficiency of the capital employed (i.e. bonded) to obtain a place in the nodes’ list is enhanced considerably when compared to the Pure  Bond Model —where the native token is being used solely as an economic guarantee and is not generating any type of yield on its own.  &#x20;

Not all assets will be bondable, either. Only relatively lower volatile external assets such as stablecoins, BTC, or ETH will be; other Bond Pools could be added with a 67% nodes’ consensus. For security reasons, it is suggested that no more than 2 assets per chain are bondable (i.e., BUSD and BNB on Binance Chain, but not any other BEP2 coins).&#x20;

This innovative alternative to the traditional bonding model simply follows the economic principles of efficient use of capital and resources. Any investor that can generate better risk-adjusted capital returns will tend to do so, and so we want to offer our operators this efficient and interesting model.  &#x20;

We should also mention here that whereas the traditional economic design and bonding requirements result in a theoretical deterministic value for $RUNE’s market cap of 3X its Total Value Locked (TVL), in Maya, $CACAO’s deterministic price will be 1X TVL. This might look lower at first glance but is actually the reflection of the higher capital efficiency within our protocol and the tighter relationship of $CACAO price to liquidity and fee generation.&#x20;

Finally, we designed a model that creates a liquidity flywheel effect while permitting similar security parameters to the legacy pendulum and brings other advantages that we can test —audits and Maya Stagenet first, of course—  for THORChain to implement if we all find them practical and successful.&#x20;

\
