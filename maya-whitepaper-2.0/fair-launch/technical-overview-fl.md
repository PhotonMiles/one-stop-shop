# Technical overview FL

### I. Liquidity Auction&#x20;

To make the Liquidity Auction work, as well as THORChain, we will use existing attributes of Mimir. These attributes will control the actions that all liquidity providers can take on a specified time frame to successfully execute our Fair Launch.&#x20;

Using Mimir Key terms, we want to accomplish the following:&#x20;

* Users should only add/withdraw liquidity.&#x20;
* Users should not be able to swap or send.
* Users should not be able to get $CACAO until the end of the Liquidity Auction.&#x20;

The process we will follow starts by enabling the new “LiquidityAuction” Mimir attribute, which stops users from being able to swap between any assets because all swaps between native assets without $CACAO would be discarded. This behavior will work for 21 days, after which the $CACAO is distributed, and “LiquidityAuction” is disabled.&#x20;

Here are the already existing transactions, along with the new ones and the specific actions that they disable:&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -28.png" alt=""><figcaption></figcaption></figure>

Distributing $CACAO tokens after the Auction process is simple and will require the use of the “Donate” message to dispense them into our pools proportionally to their depth in USD terms, using an End-of-Auction overall market price.

Any user that contributed their native assets ends up having their original assets plus their newly earned $CACAO. Any and all UI’s supporting the Maya Stagenet —and therefore our Mainnet— can host the Liquidity Auction. Code Savvy individuals may also use the API/Transaction Memos directly.&#x20;

**User stories:**

1. As a user, I should only be able to provide asymmetric liquidity throughout the Fair Launch to get $CACAO in the Liquidity Auction. Acceptance criteria:&#x20;

* Users should only add/withdraw liquidity.&#x20;
* Users should not be able to swap or send.&#x20;
* Users should not be able to get $CACAO until the end of the Liquidity Auction.&#x20;

2\. As a Liquidity Provider, I should be able to withdraw my liquidity at any point in time to recover my money if I no longer want to participate in the Auction.&#x20;

### **II. Genesis Nodes**&#x20;

Our first nodes will be called “Genesis Nodes”, and there will be six of them. Because they will start running the protocol with no $CACAO bonds —remember there will still be no $CACAO tokens until after the Liquidity Auction is finished— we will need them to already have some dependable reputation, which is why they will need to be pseudo-doxxed nodes, run by decentralized organizations close to Maya. Once our chain and systems have been started, these initial nodes will exit over time as other nodes enter the network.&#x20;

Genesis nodes will be approved using a specific custom-made token for this purpose. They will not be entitled to any fees, special allocations, or pre-mines of any kind. For more details on our Genesis Nodes, please refer to [Part 4: Security Nodes.](broken-reference)

A partition value will be created on Mimir and set initially to 1. This will override the real measured value of the[ liquidity partition](../liquidity-nodes/economic-overview-ln.md), which initially would be 0, so that yield is paid to Liquidity Providers despite the lack of bonded liquidity. The mimir set value will be gradually decreased to incentivize more node bonding, pushing the measured value higher, until they eventually equal somewhere above 75%. Once they equal, genesis nodes churn out and the system is safe and sustainable.&#x20;

**User story:**&#x20;

1\. As a genesis node, I should be able to be a validator in the chain without contributing economically and without affecting the $CACAO supply. Also, I should not get any sort of pre-mine or reward during this period.&#x20;

### **III. $RUNE**&#x20;

As well as THORChain, we will use Bifröst, a module that makes it possible to generate a native asset exchange network.&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPER IMAGES-04.png" alt=""><figcaption></figcaption></figure>

**User story:**&#x20;

1\. As a user, I should be able to add and withdraw $RUNE liquidity on Maya during and after the Liquidity Auction.&#x20;

2\. As a user, I should be able to swap $RUNE for any other asset in Maya after the end of the Liquidity Auction.

\
