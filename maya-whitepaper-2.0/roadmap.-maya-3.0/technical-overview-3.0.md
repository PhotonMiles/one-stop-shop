# Technical overview 3.0

$USa and $USb have been fully developed according to specs. They were developed directly on-chain for native functionality using Golang and the CosmosSDK. This means that they can be regularly updated, improved, patched, iterated upon and are under node governance.&#x20;

Likewise, $USm, $USs and $USc will have all their functionality developed natively. This is in line with our ethos of iterating on designs, a possibility out of reach for Smart Contracts, which have to regularly migrate all their liquidity from one place to another, making iteration complicated and even undesirable.&#x20;

Given this, a lot of how all 5 stablecoins work will likely change, with respect to descriptions in this document. For instance, taking advantage of on-chain always-on voting can lead to developing a Stop/Go voting mechanism for $USa, or caps might be implemented or lifted. Parameters can be tweaked, and new ideas can be introduced.&#x20;

The decentralized stablecoin field is still young, with plenty of space for improvement and development. These facts must be hard-coded into their implementation, into our culture and our community.

Given that $USm, $USs, and $USc are still to be developed, user stories and code herein apply exclusively to $USa and $USb.&#x20;

Although $USa and $USb codes will be audited before Maya’s fair launch, economic audits might take longer, and it will be the decision of our community to leave $USa and $USb in standby mode until our stablecoin suite is more solid. $USb can also be turned on before $USa and others to make sure it works as intended, both technically and economically, before the rest of the suite comes online.&#x20;

### User Stories:

1. $USa

* As a user, I want a liquidity-sensitive fee implemented for expatriating and repatriating $CACAO between Maya and Aztec, so the Treasury has funds to keep my investment safe.&#x20;
* As a user, I want 90% of seigniorage burnt to leave more room for $CACAO re-minting during $USa sell-off events. I also want 10% to go to the treasury, so it has funds to keep my investment safe.

2\. $USb&#x20;

* As a user, I want to be able to mint $USb by burning $USa or other stablecoins in a non-reversible way.&#x20;
* As a $USb owner, I want to receive interest payments in $USb, regardless of whether I’m staking $USb or not.&#x20;
* As a $USb owner, I want to be able to stake and unstake my $USb at will.&#x20;
* As a $USb staker, I want to receive my share of $CACAO pay downs. I need the treasury to have a way to fund the Bond Reserve for these payments to take place.

3\. Translate Bifröst

* As a user, I want to be able to bridge my stablecoin liquidity to other external assets through an Aztec Yax Bridge to Maya.
