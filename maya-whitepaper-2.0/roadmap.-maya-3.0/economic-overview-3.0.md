# Economic overview 3.0

As mentioned before, we believe in a multi-product design for our stablecoin solution, which distributes the load and risk among different mechanisms to reduce systemic risk and increase antifragility.  Above all, $CACAO supply and price must always be protected. Let's look at the economics of each one of them:&#x20;

We actually came about with one of our stablecoin’s design by accident,  while tweaking models and trying to answer some important questions… &#x20;

Can we design a fail-safe system if any of our stablecoins fail?  If one of our stablecoins is algorithmic, can we avoid $CACAO hyperinflation? As it’s often the case, we looked for inspiration in TradFi, specifically in Government Treasury Bonds.&#x20;

Treasury Bonds are a way for governments to fund themselves. They are used to raise capital for government expenditure, especially during tough times, and to relieve the pressure on taxes and inflation, as well as stimulate economic growth. In playing around with this idea, we wanted to create a Treasury Bond mechanism for Maya and Aztec. &#x20;

People would be able to convert a depegged stablecoin into a BOND object, which would represent the value owed to the owner (the full USD  $1.00, not the depegged value) and allow him or her to be paid over time by System Income of both chains, accruing interest on the unpaid principal. &#x20;

In TradFi, bonds approximate the price of their “face value” or “par value”  as they mature but can trade at a premium or at a discounted price around that price, depending on several things, like their interest rate or the present value of their future cash flows. If designed correctly, BOND’s can themselves be approximately worth what the original debt is worth…  so our fail-safe pressure relief valve is incidentally our new stablecoin:  $USb.&#x20;

#### Bond USD&#x20;

Ticker: $USb&#x20;

Nickname: Butter Dollar&#x20;

$USb is designed to protect a stablecoin holder’s value. Because it’s supposed to accrue its full value over time, it will not be precisely valued at  $1.00 USD all the time but will instead derive its value from the market conditions and Maya’s perceived creditworthiness.&#x20;

A holder of one $USb owns a virtual $1 dollar iou at present value, guaranteed  by the system’s future income. As long as the Maya Economy and network  value are active, that note is getting paid down. All of our other stablecoins  can be converted into $USb, irreversibly, if their owner decides to do so.&#x20;

**The BOND Mechanism**&#x20;

$USb tokens record a stablecoin debt to their holder, paid out in $CACAO via an interest rate every x number of blocks. This interest rate is partially set by  Nodes and can be modified by a supermajority vote (we will cover why it is only partially set later).&#x20;

Let’s see how each part of the mechanism would work in a simulated  scenario:&#x20;

Suppose a 3.717% APY, payable every 14,400 blocks (roughly every day). $USb  holders would be then getting a daily 0.01%, coming from the System  Income, in $CACAO

**Minting.** &#x20;

Anybody, at any moment, can mint $USb by burning one of our other stablecoins. The reverse process is never allowed.&#x20;

The process is not liquidity sensitive and would not incur any slip fees. Slightly different conversion rates can be used depending on the burnt stablecoin. Ex. Our overcollateralized stablecoins could be allowed to mint 1.0  $USb, while our algorithmic stablecoins would be allowed to mint only 0.99  $USb.&#x20;

**Staking.** &#x20;

$USb must be staked to receive interest payments. We need to do this to burn some of them later.&#x20;

Unstaked $USb can be transacted as any other token would. The tokens can be sent through the IBC to other Cosmos-based chains, although they would stop accruing interest.&#x20;

**Funding.** &#x20;

Whenever there is staked $USb in the Bond Reserve Pool, a consensual percentage of the System Income —Maya’s, Aztec’s, or both— is routed to the  Bond Reserve in $CACAO.&#x20;

**Proportions.** &#x20;

Every 14,440 blocks, a snapshot of the staked and unstaked $USb is taken.&#x20;

**Payout.** &#x20;

The entirety of the $CACAO in the Bond Reserve is paid out directly to the addresses that staked their $USb, proportional to their percentage of the total stake.&#x20;

This is liquid $CACAO that can be traded for any asset or otherwise used elsewhere, inside the Maya Economy.&#x20;

**Burning.** &#x20;

Paid for $USb is then burnt and removed from the Staking Pool. $CACAO’s price, denominated in USD, is fed from an oracle.&#x20;

**Paydown.** &#x20;

Staked $USb maintains the original proportions, but the Staking Pool depth has decreased.&#x20;

We use these percentages to keep accounting records for the next 14,399  blocks until the next paydown is due.&#x20;

**Interest.** &#x20;

Aztec Chain augments 0.01% of the total unstaked $USb circulating supply and distributes it to all unstaked $USb holders proportionally to their positions, essentially acknowledging the interest owed on outstanding principal.&#x20;

**Repeat.**&#x20;

After 14,400 blocks —or approximately one day— the process starts again.

**Exactly how should $USb be valued?**

1\. Market forces will dictate the market price of $USb, according to classic supply and demand (with some demand elasticity, as will be covered later). $USb’s primary market will be a $USb / $CACAO pool available in Maya. Other secondary markets are bound to exist.

2\. Staked $USb should be valued like a debt instrument to an investor, with a variable interest rate and maturity.

3\. Unstaked $USb could be valued by any long-term holder and believer of Maya Protocol as a perpetual bond, accumulating pending payments until it is staked.

4\. Unstaked $USb could also be valued, by traders, as a Coupon Bond whose face value depends on how long it remains unpaid.

In TradFi, Perpetual Bonds, Coupon Bonds, and Fixed Payment Loans are valued using the following means:

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -06.png" alt=""><figcaption></figcaption></figure>

**Discount Rate (dr):** It is the rate at which money is losing value over time due to inflation or costs of capital, an essential element to calculate the Net Present Value (NPV) of money.&#x20;

Although different investors can use the above equations to set their own discount rates and the actual loss of value for money over shorter periods of time is difficult to obtain, we believe that two useful baselines are: the Risk-Free Rate, which usually means the U.S.’ 3-month Treasury Bill rate of return, and the 12-month Secured Overnight Financing Rate (SOFR), which is updated every day by diverse global economic data providers.&#x20;

**Uncertainty Risk (U):** There are inherent risks to any investment, including bonds. Debt issued by big sovereign countries, usually considered the safest investments in TradFi, is still somewhat risky since governments can be overthrown, they can lose wars, have a popular leader decide not to pay their lenders, and more. These risks, and any other black-swan events, become more probable with longer time horizons. Maya’s equivalent risks include hacks and code vulnerabilities, a decreased demand for our product —cross-chain swaps— for any reason like only one blockchain ends up dominating the market, an overall implosion of the crypto industry, and more. Although we consider that the risk of our nodes not honoring their payments is close to zero —since they know that also means killing the network’s reputation— as long as the market considers any risks to be existent, then the Uncertainty Risk should be non-zero.&#x20;

**Coupon Rate (C) & Interest Rate:** Maya and Aztec should seek to pay an attractive Interest Rate, one that is above the Discount Rate but one that is not too high to become a burden. In the case of $USb, the Coupon Rate is the same number as the Interest Rate, and we can use them interchangeably for the sake of clarity or simplicity.&#x20;

**How Nodes set (C):** Due to its daily updates, Maya would begin using the 12-month forward SOFR as the Discount Rate, which is obtained using a Node run oracle feed; the community can also propose a better reference rate. After nodes set the Uncertainty Risk (U) constant (ex. to 1%) the Coupon Rate would be set to U + Dr. This is what was meant before with “nodes only partially set” the interest rate. If the market, during rational times, is starting to sell $USb for less than $0.99, our nodes can vote to increase U; if the market is selling $USb for more than $1.01, our nodes can vote to decrease U.&#x20;

U cannot be negative and should not be higher than 2%, as it is not meant to be a speculative investment or attract mercenary capital. If U is already 2% and the market still sells $USb below $0.99, that means that either the market is behaving irrationally or that Maya is not deemed creditworthy anymore.&#x20;

In such an event, Maya cannot rationalize the market or increase its creditworthiness by increasing its debt obligations. Therefore, if Nodes has already pulled all their available levers and $USb is off the peg, Nodes should allow $USb to continue trading downwards until the market corrects itself or Maya is deemed creditworthy again. Despite the bond not trading on par, the bond owner is, in fact, receiving their pay downs to maintain an above $1.00 present value to the holder. One of our nodes’ tools to improve our Uncertainty Risks would be to continue paying down debt, reducing the Time dimension to uncertainty and bringing $USb closer to its original peg.&#x20;

Let’s now analyze each value method carefully.&#x20;

**Mo: The Market Price.** which we can’t directly influence. Anything too far from $1.00 would mean that the real-world discount rate is different than what Nodes are reporting, that uncertainty risk is greater than projected, that the markets are acting irrationally (typically fear), and that total $USb supply is very high or any combination of the above. Nodes can indirectly influence Mo by manipulating the Coupon Rate (through changing U), as well as paying down debt more quickly, reducing time risk, default risk, and market uncertainty simultaneously.&#x20;

**Po: The Perpetual Price.** The value of the bond to a long-term and risk-averse investor that plans to hold it to virtual perpetuity. This value would always equal $1.00 if the Coupon Rate were equal to the Discount Rate. The Coupon Rate will most often be higher than the Discount Rate to account for the Uncertainty Risks that the market might perceive. Investors holding the bond in perpetuity thus hold more than $1.00 in present value.&#x20;

**Lo: The Loan Price.** the present value of the bond when it is staked by an investor who seeks to get recurring pay downs on both principal and interest. The real Loan Price is greater than $1.00 if the Coupon Rate > the Discount Rate. The faster the principal is paid, the more the Loan Price asymptotes to $1.00; the slower the principal is paid, the higher the Loan Price leans away from $1.00 (if the Coupon Rate > the Discount Rate it goes higher. If the Coupon Rate < the Discount Rate it goes lower). Given C = Dr + U, and U >= 0, the fair Loan Price will go below $1.00 only if the data used to measure the Discount Rate is wrong. If the community constantly values Loan Prices below $1.00, it strongly indicates that nodes should change how Dr is measured.&#x20;

**Bo: The Coupon Bond Price**. the present value of the bond is most likely used by Bond Traders, who are constantly seeking to maximize value and have a shorter time horizon. Traders never get paid the bonds’ Face Value by the treasury. Rather, they will either resell, hold or stake them to benefit the most from them. Traders set an expected value of L or M, which we can call Le and Me, at some point in the future, whenever they intend to realize their profit —or loss. Whenever Lo is high or Mo is low, traders will prefer to earn by staking the bonds. Whenever Mo is high or Lo is low, traders will look to unstake and sell them. N is simply the number of days between the present and the chosen point in the future. Whenever these bond traders expect changes to Le or Me, they will hold unstaked positions for N days to realize their value. Bo is by far the most subjective way to value the bond and will depend mainly on each trader’s investment thesis and future outlook.&#x20;

**Ro: The return.** we speak of Ro whenever we talk about Lo, Bo and Po interchangeably- notice that Mo is left out. It is the all-encompassing term to call the present value of the Return on the bond in general.

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -07.png" alt=""><figcaption></figcaption></figure>

Having gone through the above, we should notice 3 maxims will always hold true for $USb:

1. **If C > dr**, the present value of Ro will always be worth more than $1.00 USD.
2. **Price of Mo Minimum\[Price of each Stablecoin]**, or more interestingly, the Minimum\[Price of each stablecoin] > Price of Mo, for 2 reasons: a. If Mo goes above this value, demand for the bonds in the market dries up since it would be cheaper to mint $USb from another stablecoin which would make $USb demand decrease. b. If a stablecoin drops starkly below its peg, such that the Mo price is greater than its price, arbitrageurs can acquire that stablecoin in the market, burn in it exchanges for $USb, and sell them for $CACAO (or another asset in the market), which would make $USb supply increase.
3. **Bond demand is very elastic to Mo.** Bond demand is very elastic in relation to Mo. As the price of Mo decreases and given that the present value of Lo, Bo and Po are unchanged by market movements, the ROI an investor gets from buying Mo increases countercyclically. Essentially, the investor’s ROI = Ro / Mo, and the further that Mo depegs from $1.00 the better the ROI that a $USb investor would get. For example, if Mo is $0.50 then the investor’s ROI becomes 2x what the Ro was designed to be. We can generalize this behavior as Demand Elasticity for $USb in the market being greater than 1.&#x20;

<figure><img src="../../.gitbook/assets/WHITE PAPER RRR.png" alt=""><figcaption></figcaption></figure>

**$USb as the peg sink.**&#x20;

$USb serves as an excellent tool of last resort in the case that anyone other of our stablecoin depegs from $1.00 because, with every lost cent, it becomes more and more desirable for rational actors to buy up the troubled stablecoin, burn it in exchange for $USb and then hold, stake, or sell their new tokens, which of course increases the strained stablecoins’ demand and decreases their supply. Importantly, $USb achieves all this without ever-inflating $CACAO’s supply and without using any funds in the Treasury.&#x20;

In the case of asset-backed stablecoins (see our delicious assortment here), whenever they are burnt in exchange for $USb, they are leaving valuable assets behind in our networks… As long as those assets are more productive —or appreciate more— than the interest paid to the new $USb holders, then the network will generate profits. This means the Maya and Aztec chains are fundamentally long crypto and the multichain future!&#x20;

There would likely be a lag between the re-pegging of the affected stablecoin and the re-pegging of Mo, which would be acceptable since keeping the peg is not Maya and Aztec’s top priority. Rather, they are interested in paying back debt to all $USb stakers.&#x20;

Also, notice how the Treasury never tries to maintain the affected stablecoin’s peg artificially, it will only make sure Ro is above $1.00, and confidence in $USb is high. Instead of trying to pay off any panic sellers, the treasury only spends a small fraction of its reserves by paying $CACAO to all $USb stakers, at a reasonable pace and rate.

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -09.png" alt=""><figcaption></figcaption></figure>

**$USb makes the Maya Economy Antifragile**, merely by existing it makes depegging events less likely because it provides a safety net that ultimately makes stablecoin holders less prone to panic selling.&#x20;

Finally, we should note how the structure becomes safer the higher the system income gets and how the founding team —including investors and developers— are compensated only with this metric: we are incentivized to maximize system income instead of inflating any of our tokens’ prices. We know the first option makes $USb better, safer, and more attractive, whereas a baselessly inflated token price does exactly the opposite.&#x20;

So now that we have an antifragile system with an excellent peg sink, a correctly incentivized team, and a safety net that prevents any systemic failures, let’s see the rest of our stablecoins offering.

<figure><img src="../../.gitbook/assets/WHITE PAPERRRR .png" alt=""><figcaption></figcaption></figure>

### **Maya USD**

Ticker: $USm&#x20;

Nickname: Milk Dollar&#x20;

THORChain hit the ball off the park with their $TOR design, and $USm is our own version of it. $TOR offers a great option for arbitrageurs as the start and end of their operations and features an excellent mechanism to maintain its peg. Let’s see how $USm works and how they are different:

**How it works**

$USm is always redeemable for $1 USD worth of $CACAO. The exchange rate is calculated using the median value of the Maya’s $CACAO / exogenous stablecoin pools’ prices and does not require any oracles nor does it have mint or burn soft caps.&#x20;

Instead, there is a slip-based (liquidity-sensitive fee) virtual $CACAO / $USm Anchor Pool where larger transactions pay higher fees, to disincentivize panic selling.&#x20;

Our virtual Anchor Pool will not be as deep as the combination of all the rest of the stablecoin pools, instead only as deep as the deepest stablecoin pool. This leads to a shallower pool that is more profitable for the treasury and the first difference with $TOR’s design.&#x20;

We believe that making the pool deeper than this would be an overkill since $USm is already superior for arbitrageurs, compared to other stablecoins in the ecosystem, given their shorter block time, instant finality, requiring less time to settlement on the Yax Bridge (Bifröst), cheap on-chain transaction fees, etc.&#x20;

$USm main economic use is fast entry and fast exit, with a stable value between the two events. Velocity is more important than cost for most arbitrage opportunities. This small change would keep $USm supply lower and more manageable, and higher revenues from the virtual Anchor Pool would go to the treasury.

**LP Loans.**&#x20;

Anyone can borrow $USm using LP units from our bluechip pools as collateral. There will be no single side collateral in Maya to avoid a “Top-Heavy” design. Loans will start at a very competitive 110% Collateralization Ratio, which will increase as further loans of the same asset are created, to prevent overleverage on only one asset. These loans will carry a 0% interest rate given that the collateralized LP Units are productive for the ecosystem.

**IBC**&#x20;

It will be possible to send $USm into the Aztec Chain although it will have to pay a second liquid sensitive fee when returning into the Maya Chain. It won’t be possible to send $USm into other chains, through IBC, since its main use is for arbitrage inside Maya and that demand driver should be good enough.

**Maya Pool.**&#x20;

$USm sent through IBC to the Aztec Chain could be LP’d into Maya through the Yax Bridge. This Maya Pool will serve to capture liquidations without using order books.

**Liquidation Allocation.**&#x20;

Whenever a loan goes below the collateralization ratio, a liquidation event is triggered.&#x20;

Every pool has a Liquidation Allocation (LA) variable, set between 0% and 100%. If LA.BTC is set to 70%, which means that a liquidation event on Bitcoin will make 70% of the LP Units to remain as Protocol Owned Liquidity —which serves as a liquidity provider of last resort— and 30% to be withdrawn asymmetrically into $CACAO that then buys $USm from the corresponding pool.&#x20;

Acquired $USm is burnt (settling the protocol debt), unless burning all of it would burn more $USm than what was minted in the loan. In this second case, profits are kept as $CACAO in the treasury, and only enough $USm is burnt to settle protocol debt.&#x20;

Theoretically, liquidations should always represent a profit for the system given that the Collateralization Ratio (CR) would always be 110% or more. The more volatile or risky a pool is, the closer to 0% the Liquidation Allocation should be, conversely, the less volatile or risky the pool (ex. BTC or Stablecoin pools), the closer to 100% the Liquidation Allocation can be. This parameter is set by the nodes’ consensus.

**Liquidity Stickiness**&#x20;

Most of the liquidity in Maya is bonded by the nodes, thanks to our Liquidity Nodes functionality. This means our liquidity is sticky which makes $USm safer.

**No $CACAO minted above total supply.**&#x20;

Even after the security mechanisms for $USm, like a shallower pool, price penalization for panic sellers and a safe exit into $USb, there is still a risk of depeg if Maya burnt $CACAO Market Cap diminishes and comes close to $USm’s market cap. Because $CACAO cannot be re-minted above the Total Supply, in this case a $USm holder can only:

1. Hold until the peg is recovered.
2. Sell $USm at a loss.
3. Convert to $USb.&#x20;

Massive use of t third option could then cause a depegging of Mo with $USb, and then the user’s could only:

1. Hold until the peg is recovered.
2. Sell $USb at a loss.
3. Stake their $USb to get interest payments over time.&#x20;

### **Aztec USD**&#x20;

$USa is our version of $UST, an algorithmic, decentralized stablecoin pegged to the USD that could be used for permissionless foreign exchange trading, remittances and more. We all know what, unfortunately, happened to the Terra / LUNA project, where demand for $UST increased disproportionately, in unproductive and unfavorable ways for the protocol. Let’s see how $USa works and how it iterates over the lessons that $UST left us with:

**Liquidity Sensitive Fees**

$USa is minted with $CACAO, which is a productive asset, with real use cases outside of the Aztec chain. This is a stark difference with the Terra / LUNA model and is extremely important because it limits death spiral risks.&#x20;

$CACAO is productive because moving it around between chains and using it to provide liquidity generates revenues for the system. The system’s treasury can use these revenues in economically smart ways and can use them to pay down outstanding debt, held by $USb owners.

**Most seigniorage burnt from the beginning**&#x20;

90% of the seigniorage is burnt from the very beginning, giving more room for new $CACAO to be burnt into $USa; the remaining 10% is kept in its entirety by the treasury.

**No Artificial Demand**

Maya and Aztec will never provide funds, let alone treasury funds, to drive up demand artificially. It’s better to grow slowly and sustainably, rather than aggressively but with a time bomb. There will be efforts by the team to find more uses and demand for $USa on-chain, especially for its main use as remittances and foreign exchange, but that is all.

**IBC**&#x20;

It won’t be possible to send $USa into other chains, through IBC; extra demand would only increase stress on the system plus this way it stays closer to its primary market and core functions.

**No $CACAO minted above imported supply**

Even after the security mechanisms for $USa, like liquidity sensitive fees, price penalization for panic sellers and a safe exit into $USb, there is still a risk of depeg if and when Aztec burnt $CACAO Market Cap diminishes and comes close to $USa’s market cap. Because $CACAO cannot be re-minted above the Total Supply, in this case, a $USa holder has the following options:

1. Hold until the peg is recovered.
2. Sell $USa at a loss.
3. Convert to $USb.&#x20;

The massive use of the third option could then cause a depegging of Mo with $USb, and then the user has these options:

1. Hold until the peg is recovered.
2. Sell $USb at a loss.
3. Stake their $USb to get paid down over time.

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -11.png" alt=""><figcaption></figcaption></figure>

### Synth USD&#x20;

Ticker: $USs&#x20;

Nickname: Sugar Dollar&#x20;

$USs is our own version of $DAI, which was a DeFi breakthrough. MakerDAO developed what is arguably the best-decentralized stablecoin model to date, with many diverse use cases and billions in locked value. $DAI does have some scalability problems, as will $USs but, let’s see what makes them different:

**Tendermint**&#x20;

Having instant finality, short block time, low transaction costs, and no need to burn a governance token is in itself already a big leap forward.

**Supported collateral**

$USs will allow for $sBTC, $sETH and $CACAO collateral only. $USs will not use centralized stablecoins as collateral, another big leap.

**Supply and demand levers**

A Stability Fee of 1% to 3% will be implemented and charged to borrowers. This fee will be used to stimulate demand or cool down supply without subsidizing yields artificially via the “Aztec Holding Rate” (AHR), which is paid to lenders/savers/stakers of $USs. Because not all of the $USs holders will be staked, leftover fees would be kept by the Treasury as profit.

**Liquidations through Maya Pools**

Given that both $CACAO and Synths are always tradeable inside Maya Pools and that $USs itself will be available in a pool through the Aztec Yax Bridge, liquidations do not have to go through a complex auctioning process.&#x20;

Simply, the Aztec treasury can send back liquidated synths / $CACAO through IBC to the Maya Treasury, which will use aggregated instructions to exchange $CACAO and synths to $USs inside Maya Pools.&#x20;

The principal of the loan will be burnt, settling the debt. Leftover $USs - considered profit - can be accumulated to then buy back $CACAO and Synths such that the treasury holds 50% of its portfolio in $USs and 50% in the average weighted collateral basket.

**Collateralization Ratios**&#x20;

Tendermint capabilities and liquidations happening inside Maya Pools mean that liquidation speed is increased greatly and liquidation risks are reduced greatly. This allows us to offer lower Collateralization Ratios than $DAI does —especially for $CACAO, which must only go through 1 pool to liquidate into $USs (Synth to $USs is essentially two swaps).&#x20;

Also, given the nature of the Maya pools and their slip-based fees, it is way cheaper to liquidate smaller transactions than it is to liquidate bigger transactions. In light of this, Collateralization Ratios can be as low as 120% for a small position in $CACAO and up to 150% for a big position in Synths.&#x20;

Finally, collateralization ratios can be lowered by the nodes to incentivize more $USs minting or increased to achieve the opposite effect.

**Limits**

There is no need to implement further limits to the $USs system, given that $CACAO numbers are already limited by the Max Debt parameter in the Aztec Chain and that minting Synths is limited to 16.5% of Maya Pool depth.

**Depegging downward**

When $USs is below $0.99, the treasury can swap its Synth and $CACAO portfolio back to $USs while increasing the stability fees to decrease supply and the AHR to increase demand. If the treasury runs out of Synths and $CACAO, nodes would proceed to increase the collateralization ratios, for example, $CACAO at a maximum of 140% and Synths at a maximum of 150%. If $USs keeps depegging, nodes can vote on a global settlement where $1 USD of collateral is shared to $USs holders proportional to its basket and the rest of the collateral is kept by the treasury, benefiting those who held $USs or bought $USs below the peg, and penalizing those who sold below the peg. If $USs depegs even further, converting it into $USb becomes more appealing, which, as we have seen before, generates demand for it and decreases supply.

**Depegging upwards**&#x20;

Whenever $USs trades above $1.01, the treasury can swap its $USs for Synths and $CACAO while lowering the stability fees to increase supply and the AHR to decrease demand. If the treasury runs out of $USs to swap, nodes can decrease collateralization ratios, for example, $CACAO to a minimum of 120% and Synths to a minimum of 130%.&#x20;

If $USs keeps depegging upward, nodes can vote on a global settlement, where $1 USD of collateral is shared to $USs holders proportional to its basket, and the rest of the collateral is kept by the treasury, benefiting those who bought at the peg and penalizing those who bought above the peg.&#x20;

**Productive Collateral**

During the whole depegging event, Synth collateral has been actively producing yield for Nodes and LPs in Maya, as well as the Maya Treasury, if a non-zero amount of fees from yield is allocated to it. Collateral is never simply sitting in a vault; rather it is used to stimulate the Maya Economy. Likewise, $CACAO collateral means a reduced $CACAO circulation inside Aztec, which also means less $CACAO is available to mint into $USa. If near or above the Max Debt Limit, $CACAO price in Aztec Chain increases and provides an arbitrage opportunity for the Maya Treasury.

**IBC**

It will be possible to send $USs into other Cosmos-based chains via IBC. In fact, we will actively spread it into other ecosystems, centralized or decentralized. Where $USm and $USa can mostly have demand issues —too much of it—, $USs faces the opposite problem, so increasing demand with this means will help it get adoption and alleviate pressure on our other stablecoins, mainly $USm and $USa.

**Governance**&#x20;

There is no complex governance structure for $USs. Nodes have skin in the game, and they are deeply involved in the Maya and Aztec chains, so they get to decide on these complex issues. $CACAO users can vote with their money by buying or not buying into $USs.&#x20;

Nodes must be honest, fair, and above all, communicative to the Community to drive up $USs adoption, which is best in their interest.&#x20;

### Collateralized USD&#x20;

Ticker: $USc&#x20;

Nickname: Chocolate Dollar&#x20;

**$USc, and $UScc, our versions of $USDC.**&#x20;

Creating a 1:1 collateralized and decentralized stablecoin is very difficult… Collateralizing with fiat would eventually get you into trouble with Uncle Sam, and collateralizing with other somehow centralized assets would only move the problem to another layer, which doesn’t really solve anything. People have tried multi-sig solutions, DAOs, and smart contract escrows but central planning keeps leaving its mark and creating vulnerable points of failure. We believe that $USc and $UScc can creatively sidestep these issues, let’s see how:

**Collateral**&#x20;

Both $USc and $UScc work exactly the same way: by tracking the price of a basket of other stable pools. $USc tracks a basket composed of our other stablecoins —$USm, $USa, $USs, and $USb— while $USc tracks a basket that also allows for synthetic and external Cosmos-based stablecoins. Oh, by the way, did you notice how our Milk dollar + our Almond dollar + our Sugar dollar + our Butter dollar form our Chocolate dollar? Yes, we really are geeks too.&#x20;

$USc does not need to rely on any third-party dependency or project while $UScc has a higher risk diversification.

**Basket**

$USc holders themselves are best suited to decide on this! We will implement an “always on” vote mechanism for each wallet address, where a simple transaction with four numbers is sent to indicate the preferred stablecoin allocation. One of these transactions might look like this: “Portfolio:20:10:45:25”, which would indicate the holder suggests his positions should be backed up by 20% of $USm, 10% of $USa, 45% of $USs and 25% of $USb. These preferences would reflect the market outlook and situation.&#x20;

The vote is always on and can be overwritten at any time by doing a new Set Portfolio transaction. Every address’s vote is multiplied by the number of full $USc that they hold. Wallets holding less than 1 $USc get their votes nullified. If the wallet has never set a Portfolio Split, its vote is nullified. All votes are tallied, and the median value of each asset is chosen. Because the sum of all 4 medians will most likely not be equal to 100, they must be normalized to 100 simply by dividing by the sum of the medians.

**Minting**&#x20;

There is a short 20-minute delay in minting and burning $USc because

a. $USc is not a fast exit liquidity stablecoin. It is instead meant to steadily hold its value.&#x20;

b. They are competing against “several business days” centralized stablecoin issuers.&#x20;

During this delay, users can post more collateral in any proportion. After the delay, Aztec will issue as many $USc / $UScc as the collateral proportion allows, trying to pair any unbalanced collateral with other users using FIFO. Any stablecoins that the system could not pair during the delay will be readily refunded.

**Redeeming**

When redeeming $USc, the user will always get back the real portfolio ratios worth of Aztec stablecoins. Again, there is a 20-minute delay in redeeming.

**Changing the basket**

When the basket changes, simply the pairing for new $USc minting changes, whereas redeeming is always done with the real portfolio ratio, not the ideal set ratio. Therefore, real ratios change slowly only as people continue to redeem and mint.

**Limits**

There are no limits to the minting of $USc itself, but since other stablecoins (except for $USb) have their own limits, it will be limited to how much collateral is available to mint $USc.

**Global Settlement**

Just like with $USs, a global settlement event can occur if $USc or $UScc market prices go significantly above or below the peg.

**Profits and assets left**

The small minting fee will be kept in $USc by the protocol to accumulate it and hold it. Notice $USc helps stabilize the whole ecosystem also since it is a source of demand for all stablecoins (with the load shared between all of them) and locks these stablecoins off from being sold off quickly in panic sales. $USc can also lose the peg while collateral remains healthy, creating buy-and-burn demand for $USc to create $USb. These transactions leave $USc and, therefore, collateral behind, which is now owned by Aztec Treasury.

**Collateral drop**&#x20;

Since any stablecoin’s drop likely causes a drop in $USb price as well, there is always a risk that 2 out of the 4 collateral types fall together (albeit a smaller fall than if $USb did not exist). This would cause the $USc price to fall also, albeit in a smaller magnitude given that it holds the other 2 stablecoins as well, causing one of three outcomes for every user: 1. Those who hold and wait until peg is regained by the underlying collateral and therefore $USc 2. Those who choose to redeem and realize their loss or 3. Those who choose to convert to $USb, leaving their collateral behind to the Aztec Treasury. If the depeg persist and $USc is therefore still significantly off the peg, $USc holders will most likely change their on-chain vote to reduce exposure of the depegged assets significantly, thus slowly changing the collateral portfolio composition to a better pegged combination as some users redeem and and others re-mint.

**Opportunistic minting of $USc**&#x20;

When a stablecoin is severely depegged, and $USc has exposure to it, a situation can happen where a holder of a depegged stablecoin can attempt to mint $USc at a profit using only the depegged coin. To stop this, Nodes can vote to disable disproportional minting of $USc during crisis times, requiring users to post perfectly proportional split of the different stablecoins to mint $USc, according to the latest basket split. This behavior could be automated carefully so no Node vote is required.

**IBC**&#x20;

$USc will be IBC-enabled and proliferated in other Cosmos Based chains and other centralized and decentralized ecosystems. As our Chocolate USD, and first 1-to-1 backed decentralized stablecoin, it will be our flagship token for stability in off-chain uses. The team will actively seek to build bridges for $USc to other ecosystems like Ethereum and Solana. Importantly, $USc can have high demand given that it shares the load among different failure modes and its portfolio composition will most likely be voted to be inversely proportional to each stablecoin’s risk. Finally, it will be the only Aztec stablecoin to not have a $CACAO/$USc Maya Pool, for reasons that will be outlined in [Part 6.](broken-reference)&#x20;

### Final remarks&#x20;

We believe our stablecoins’ designs have several advantages, in addition to the way it distributes the demand load between all of them:&#x20;

$USc, which becomes our major stablecoin, re-balances continuously, sharing all of the other stablecoins’ advantages and disadvantages. It also motivates disclosure, critical thinking and transparent discussion about the underlying strengths and weaknesses. This also helps our users ultimately decide on what tokens to use, based on their needs or investment thesis, and considering their desired risk / reward ratios.&#x20;

All of our designs are also of course subject to iteration and failure, but are well covered for the latter with our Bond mechanism. Our stablecoins can and will grow as much as they are demanded.&#x20;

Finally Maya Pools are a set of Continuous Liquidity Pools, run by the same nodes that keep Aztec secure, and that also have a huge vested interest in the well-being of $CACAO and the whole of the network.

<figure><img src="../../.gitbook/assets/WHITE PAPER GRAPHICS -12.png" alt=""><figcaption></figcaption></figure>
