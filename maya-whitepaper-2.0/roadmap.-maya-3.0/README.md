---
description: Not all stablecoins are created equal
---

# 👣 Roadmap. Maya 3.0

The search for a decentralized stablecoin has been a tough one, and unfortunately,  many attempts have failed to this date. In  Maya, we believe that there is no such thing as an optimal universal model for stablecoins, one that rules them all… we believe that diversification of risks is key for stability, and we are building with this idea in mind.&#x20;

We have named this Roadmap: Maya 3.0  since everything in this Whitepaper after this section will not be available at launch since it requires further work, design, code, and economic audits, among other things. In other words, everything before Maya 3.0 will be available at launch after the Liquidity  Auction takes place. Turning Maya 3.0 on or leaving it off forever will be decided by community discourse and Nodes when the time comes if the proposed design makes sense and has gone through the necessary audit and community review processes.

### &#x20;ELI5

1\. Decentralized stablecoins have been called one of the “Holy Grails” of crypto, and our industry has experimented with different approaches and models to work them out. Today, Algorithmic Stablecoins are still risky, and Overcollateralized Stablecoins are still tough to scale and capital-inefficient.&#x20;

2\. By looking at previous experiments, we believe that stablecoin models have always assumed they would succeed. Instead, we believe in iterative design and have approached the issue with a “fail-safe” attitude, where the risks of depeg always exist and need to be balanced and offset in more than one way.&#x20;

3\. Maya will have 5 different stablecoins in its suite. Each one will be best suited for a specific type of user/investor since all of them have their own strengths and weaknesses, which will be clearly laid out at all times to everybody.
