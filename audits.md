---
description: >-
  Find detailed information about the various security and code audits that have
  been conducted on the protocol.
---

# Audits

## Halborn

* Draft: [ETH Router](https://maya-cdn.s3.amazonaws.com/Halborn/ETH\_Router\_Draft\_3.pdf)
* Final: [Layer 1 & Liquidity Nodes](https://maya-cdn.s3.amazonaws.com/Halborn/Cosmos\_Security\_Final.pdf)
* Final: [Liquidity Auction](https://maya-cdn.s3.amazonaws.com/Halborn/Liquidity\_Auction\_Final.pdf)
* Final: [Dynamic Inflation](https://maya-cdn.s3.amazonaws.com/Halborn/Dynamic\_Inflation\_Final.pdf)
* Final: [Liquidity Auction Tiers](https://maya-cdn.s3.amazonaws.com/Halborn/Liquidity\_Auction\_Tiers\_Final.pdf)
* Final: [Thorchain's Bifrost](https://maya-cdn.s3.amazonaws.com/Halborn/TC\_Bifrost\_Final.pdf)
* Final: [Dash's Bifrost](https://maya-cdn.s3.amazonaws.com/Halborn/Dash\_Bifrost\_Final.pdf)
