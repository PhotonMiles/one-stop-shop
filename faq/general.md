# General

**What is Maya Protocol?**

Maya is a decentralized liquidity protocol that enables the fast and efficient exchange of cryptocurrencies across different chains without intermediaries, bridges, or wrapping. Maya's technical design is based on THORChain, to whom we credit the impressive feat of first designing and building a technically viable, completely decentralized, permissionless exchange.&#x20;

**What can I do in Maya Protocol?**

Maya has several different functionalities. According to your needs, risk profile, and technical expertise, you can:

a) Swap: Trade, buy, or sell different cryptocurrencies without intermediaries.

b) Provide Liquidity and earn yield: Support our liquidity pools by contributing your assets and generating passive income.

c) Run a Node or a Validator: Help secure our network in exchange for fees and yield.

**What is the difference between Uniswap and Maya?**

Uniswap, like Maya, offers a decentralized way to exchange cryptocurrencies. However, it only allows trading inside the Ethereum chain between ERC-20 tokens (i.e., you can't trade native assets from other blockchains, only representations of them called "wrapped" or "synthetic" tokens).

Maya allows exchanges between native currencies in their respective blockchains, reducing attack vectors and counterparty risk for users and liquidity providers.
