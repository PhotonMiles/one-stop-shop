# Swaps

**Do I need $CACAO to swap tokens on Maya?**

You do not need $CACAO to interact with MAYAChain. You can perform swaps and add/remove liquidity without directly touching $CACAO or the MAYAChain protocol. You will need to find a MAYAChain-connected wallet.

**Is there a Network Fee?**

Yes, the Network Fee is collected in $CACAO. If the transaction involves an asset that is not $CACAO, the user pays the asset's Network Fee. The equivalent amount is taken from that pool's $CACAO supply and added to the Protocol Reserve.

**What assets are available to swap?**

Users can swap any assets which are on connected chains and which have been added to the network. Users can swap from any connected asset to any other connected asset. They can also swap from any connected asset to $CACAO.

**Are swaps decentralized?**

MAYA manages the swaps following the rules of the state machine - which is completely autonomous. Every swap that it observes is finalized, ordered, and processed. Invalid swaps are refunded, and valid swaps are ordered transparently and resistant to front-running. Validators can not influence the order of trades and are punished if they fail to observe a valid swap.

**How long does it take to complete a swap?**

Swaps are completed as fast as they can be confirmed, around 5-10 seconds.

**Are there any costs to swap?**

The cost of a swap is made up of two parts:

1. Outbound Fee
2. Price Slippage

All swaps are charged a network fee. The network fee is dynamic – calculated by averaging recent gas prices. Learn more about [Network Fees](https://www.notion.so/general-docs/how-it-works/fees#network-fee).

Note that users who force their swaps through quickly cause large slips and pay larger fees to liquidity providers.
