# Liquidity Providers

**What does it mean to be a Liquidity Provider?**

Liquidity providers deposit their assets in liquidity pools and earn yield in return. They earn tokens in Cacao and the pool's connected asset. For example, someone deposited in the BTC/CACAO pool will receive rewards in BTC and CACAO.

Read our whitepaper for a detailed breakdown of being a Liquidity Provider on Maya.

**Can anyone participate in liquidity pools?**

Yes! Depositing assets on MAYAChain is permissionless and non-custodial.

**What factors affect how much yield I get from LPing?**

Several factors will affect how much yield you receive, including your ownership % of the pool, swap volume, and size of swaps. The yield comes from fees and rewards from the protocol.

**How am I compensated for providing LP?**

Yield is calculated for liquidity providers in every block. Yield is paid out to liquidity providers when they remove assets from the pool. Rewards are calculated according to whether or not the block contains any swap transactions. If the block contains swap transactions, the amount of fees collected per pool sets the number of rewards. If the block doesn't have trades, the amount of assets in the pool determines the rewards.

**Is there a lockup period if I provide Liquidity?** There is no minimum or maximum time or amount. Join and leave whenever you wish. There is, however, a required confirmation time before MAYAChain credits you with liquidity when adding or swapping to protect against Reorgs. Wait time is based on: Total Asset amount (in a specific block of a specific blockchain)Per block reward of the chain.

Block Required time confirmations = Total Asset Amount (in a specific block of a specific blockchain) divided by the chains per block reward. Then that is multiplied by block time to get the approx "wait time.” See [Confirmation-Counting](../maya-protocol-docs/broken-reference/).

**What is the difference between asymmetric and symmetric LP deposits?**

Symmetrical deposits are where users deposit an equal value of 2 assets to a pool. For example, a user deposits $500 of BTC and $500 of CACAO to the BTC/CACAO pool.

Asymmetrical deposits are where users deposit unequal values of 2 assets to a pool. For example, a user deposits $2000 BTC and $0 CACAO to the BTC/CACAO pool. In the backend, the member is given ownership of the pool that considers the slip created in the price. The liquidity provider will end up with <$1000 in BTC and <$1000 in CACAO. The deeper the pool, the closer to a total of $2000 the member will own.

**Can I withdraw LP symmetrically after depositing an asset asymmetrically?**

No. You cannot withdraw symmetrically. You can withdraw only asymmetrically for LP you deposited asymmetrically.

**What is** **Impermanent Loss Protection (ILP), and does Maya offer it?**

Currently, yes. Our ILP guarantees that LPs will either make a profit or at least recoup their investment (in USD value) in comparison to holding them externally after a certain period (currently set to 100 days) and even that they can be partially reimbursed for any impermanent losses incurred before that time. Any potential losses in the LP deposit are subsidized in $CACAO.

**Are** **there any strategies for Liquidity Providers?**

There are two main strategies; active and passive.

**Passive** liquidity providers should seek out pools with deep liquidity to minimize risk and maximize returns.

**Active** liquidity providers can maximize returns by seeking pools with shallow liquidity but high demand. This demand will cause greater slips and higher fees for liquidity providers.
