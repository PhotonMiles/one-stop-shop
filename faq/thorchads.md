# THORChads

**Is there a $CACAO airdrop for $RUNE holders?**

No, the only way to get $CACAO is through the Liquidity Auction, but $RUNE owners qualify for $MAYA token airdrop. We are a friendly fork of THORChain and have no interest in vampire away any of their capital or users. We plan to share 7% of the total $MAYA token supply with them to acknowledge their support for THORChain, which makes Maya Protocol possible. Find all information related to Thorchads airdrop [here](https://www.mayaprotocol.com/latest-post/the-maya-airdrop-for-rune-holders).

**How do you qualify for the $MAYA Airdrop for Thorchads?**

7% of the total $Maya supply will go to RUNE owners simply by:

**A.** Having $RUNE bonded in a Node,

**B.** Having $RUNE locked in a symmetrical LP position,

**C.** Having a $RUNE asymmetrical LP position,

**D.** Holding $RUNE in a wallet and/or,

**E.** Holding $RUNE in Maya pools during the auction.

Note: To ensure that only “fresh” capital is attracted during our launch (i.e., no capital leaving THORChain), we designed a ruleset of snapshots to encourage Thorchads to hold their $RUNE positions or even increase them. Find all details about snapshots [here](https://docs.mayaprotocol.com/readme/usdmaya-token/technical-overview-mt).

**What wallets qualify for Rune owner's $Maya airdrop?**&#x20;

Non-custodial wallets such as Trust Wallet, XDEFI, Ledger, ThorWallet DEX. Note that Custodial wallets such as Binance and Coinbase don’t qualify.

**Any Rune sent today to a Thorwallet would qualify?**&#x20;

All RUNE sent to a Thorwallet before March 6 would qualify.

**When is the $MAYA Airdrop taking place?**&#x20;

50 days after the launch.

**How will I claim my $MAYA tokens?**&#x20;

To claim $MAYA tokens, you will have to create a Maya wallet (THORWallet can do this for your) and send a minimum of 1 $RUNE to the Maya pool. We will be publishing instructions separately so be sure to follow our official Twitter or read our #Announcements channels on Discord.

