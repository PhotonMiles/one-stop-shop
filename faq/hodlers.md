# Hodlers

**What is $CACAO?**

$CACAO is our flagship token, and we will have 100M of them. They will all be minted at once, and 90% of the total supply will be distributed in the Liquidity Auction. The remaining 10% will be allocated to the Impermanent Loss Protection treasury.

Aside from being required to run a node, they can be paired against other assets inside our liquidity pools to earn a percentage of the transaction fees generated by swaps.

[Read more](https://mayaprotocol.gitbook.io/whitepaper/readme/usdmaya-token/economic-overview-mt) in our Whitepaper.

**What sort of value accrual does the $CACAO token have?**

THORChain's design and bonding requirements result in a theoretical deterministic value for $RUNE’s market cap of 2-3X its Total Value Locked (TVL). In Maya, $CACAO’s deterministic price will be 1X TVL. This might look lower at first glance but reflects the higher capital efficiency within our protocol and the tighter relationship of $CACAO price to liquidity and fee generation.

**What is $MAYA?**

$MAYA tokens can be used to participate in our protocol's total revenues, and there are exactly 1M of them. They serve as our initial stages' funding mechanism and, by design, keep incentives for our developer team to continue their hard work in the short-term and long term.

$MAYA captures 10% of all the protocol revenue. Those who hold $MAYA tokens will be distributed $CACAO every 24 hours to the wallet that holds the $MAYA token. For every 9$ earned by LPs/nodes, $MAYA holders, including founders/devs, earn 1$, incentivizing long-term growth and value accrual.

**How will $MAYA Airdrop work?**

7% to Rune Owners. 7% to Early Nodes. 7% to Tier 1 Liquidity Providers. 1% to Maya Mask NFT Holders (80% built into the Maya Mask, 20% airdropped as tokens)

78% Dev Fund.

For detailed information, visit the [$MAYA token ](../maya-whitepaper-2.0/usdmaya-token/)section.&#x20;

**Will there be a liquidity pool to trade $MAYA?**

There will be no liquidity pool for $MAYA, and it will not be tradeable on Maya Protocol. It is meant to represent the protocol revenue and is not easily tradeable. Another DEX/CEX could list $MAYA for trading if they desire.
