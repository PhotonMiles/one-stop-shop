---
description: Make a cross-chain swap on THORChain in less than 5 minutes.
---

# Quickstart Guide

### Introduction

THORChain allows native L1 Swaps. On-chain [Memos ](../../../../maya-protocol-devs/mayachain/concepts/transaction-memos.md)are used instruct THORChain how to swap, with the option to add [price limits](quickstart-guide.md#price-limits) and [affiliate fees](quickstart-guide.md#affiliate-fees). THORChain nodes observe the inbound transactions and when the majority have observed the transactions, the transaction is processed by threshold-signature transactions from THORChain vaults.

Let's demonstrate decentralized, non-custodial cross-chain swaps. In this example, we will build a transaction that instructs THORChain to swap native Bitcoin to native Ethereum in one transaction.

{% hint style="info" %}
The following examples use a free, hosted API provided by [Nine Realms](https://twitter.com/ninerealms\_cap). If you want to run your own full node, please see [Connecting to THORChain](../../../../maya-protocol-devs/mayachain/concepts/connecting-to-thorchain.md).
{% endhint %}

### 1. Determine the correct asset name.

THORChain uses a specific [asset notation](../../../../maya-protocol-devs/mayachain/concepts/transaction-memos.md). Available assets are at: [Pools Endpoint.](https://mayanode.mayachain.info/thorchain/pools)

BTC => `BTC.BTC`\
``ETH => `ETH.ETH`&#x20;

{% hint style="info" %}
Only available pools can be used. (`where 'status' == Available)`
{% endhint %}

### 2. Query for a swap quote.

{% hint style="info" %}
All amounts are 1e8. Multiply native asset amounts by 100000000 when dealing with amounts in THORChain. 1 BTC = 100,000,000.
{% endhint %}

**Request**: _Swap 1 BTC to ETH and send the ETH to_ `0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430`.

[https://mayanode.mayachain.info/thorchain/quote/swap?amount=100000000\&from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430](https://mayanode.mayachain.info/thorchain/quote/swap?amount=100000000\&from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430)

**Response**:&#x20;

```json
{
  "expected_amount_out": "1344935518",
  "fees": {
    "affiliate": "0",
    "asset": "ETH.ETH",
    "outbound": "480000"
  },
  "inbound_address": "bc1qlccxv985m20qvd8g5yp6g9lc0wlc70v6zlalz8",
  "inbound_confirmation_blocks": 1,
  "inbound_confirmation_seconds": 600,
  "memo": "=:ETH.ETH:0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430",
  "outbound_delay_blocks": 136,
  "outbound_delay_seconds": 2040,
  "slippage_bps": 41
}
```

_If you send 1 BTC to `bc1qlccxv985m20qvd8g5yp6g9lc0wlc70v6zlalz8` with the memo `=:ETH.ETH:0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430`, you can expect to receive `13.4493552` ETH._&#x20;

_For security reasons, your inbound transaction will be delayed by 600 seconds (1 BTC Block) and 2040 seconds (or 136 native THORChain blocks) for the outbound transaction,_ 2640 seconds all up_. You will pay an outbound gas fee of 0.0048 ETH and will incur 41 basis points (0.41%) of slippage._

{% hint style="info" %}
Full quote swap endpoint specification can be found here: [https://mayanode.mayachain.info/thorchain/doc/. ](https://mayanode.mayachain.info/thorchain/doc/)

See an example implementation [here.](https://replit.com/@thorchain/quoteSwap#index.js)
{% endhint %}

If you'd prefer to calculate the swap yourself, see the [Fees ](../../../../maya-protocol-devs/mayachain/examples/fees-and-wait-times.md)section to understand what fees need to be accounted for in the output amount. Also, review the [Transaction Memos](../../../../maya-protocol-devs/mayachain/concepts/transaction-memos.md) section to understand how to create the swap memos.&#x20;

### 3. Sign and send transactions on the from\_asset chain.

Construct, sign and broadcast a transaction on the BTC network with the following parameters:

Amount => `1.0`

Recipient => `bc1qlccxv985m20qvd8g5yp6g9lc0wlc70v6zlalz8`

Memo => `=:ETH.ETH:0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430`

{% hint style="warning" %}
Never cache inbound addresses! Quotes should only be considered valid for 10 minutes. Sending funds to an old inbound address will result in loss of funds.
{% endhint %}

### 4. Receive tokens.

Once a majority of nodes have observed your inbound BTC transaction, they will sign the Ethereum funds out of the network and send them to the address specified in your transaction. You have just completed a non-custodial, cross-chain swap by simply sending a native L1 transaction.

## Additional Considerations

{% hint style="warning" %}
There is a rate limit of 1 request per second per IP address on /quote endpoints. It is advised to put a timeout on frontend components input fields, so that a request for quote only fires at most once per second. If not implemented correctly, you will receive 503 errors.
{% endhint %}

{% hint style="success" %}
For best results, request a new quote right before the user submits a transaction. This will tell you whether the _expected\_amount\_out_ has changed or if the _inbound\_address_ has changed. Ensuring that the _expected\_amount\_out_ is still valid will lead to better user experience and less frequent failed transactions.
{% endhint %}

### Price Limits

Specify _tolerance\_bps_ to give users control over the maximum slip they are willing to experience before cancelling the trade. If not specified, users will pay an unbounded amount of slip.

[https://mayanode.mayachain.info/thorchain/quote/swap?amount=100000000\&from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430\&tolerance\_bps=100](https://mayanode.mayachain.info/thorchain/quote/swap?amount=100000000\&from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430\&tolerance\_bps=100)

```json
{
  "expected_amount_out": "1344919036",
  "fees": {
    "affiliate": "0",
    "asset": "ETH.ETH",
    "outbound": "480000"
  },
  "inbound_address": "bc1qlccxv985m20qvd8g5yp6g9lc0wlc70v6zlalz8",
  "inbound_confirmation_blocks": 1,
  "inbound_confirmation_seconds": 600,
  "memo": "=:ETH.ETH:0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430:1342846539",
  "outbound_delay_blocks": 136,
  "outbound_delay_seconds": 2040,
  "slippage_bps": 41
}
```

Notice how a minimum amount (1342846539 / \~13.42 ETH) has been appended to the end of the memo. This tells THORChain to revert the transaction if the transacted amount is more than 100 basis points less than what the _expected\_amount\_out_ returns.

### Affiliate Fees

Specify affiliate\__address_ and _affiliate\_bps t_o skim a percentage of the _expected_\__amount\_out._

[https://mayanode.mayachain.info/thorchain/quote/swap?amount=100000000\&from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430\&affiliate=thorname\&affiliate\_bps=10](https://mayanode.mayachain.info/thorchain/quote/swap?amount=100000000\&from\_asset=BTC.BTC\&to\_asset=ETH.ETH\&destination=0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430\&affiliate=thorname\&affiliate\_bps=10)

```json
{
  "expected_amount_out": "1344829474",
  "fees": {
    "affiliate": "1346656",
    "asset": "ETH.ETH",
    "outbound": "480000"
  },
  "inbound_address": "bc1qlccxv985m20qvd8g5yp6g9lc0wlc70v6zlalz8",
  "inbound_confirmation_blocks": 1,
  "inbound_confirmation_seconds": 600,
  "memo": "=:ETH.ETH:0x3021c479f7f8c9f1d5c7d8523ba5e22c0bcb5430::thorname:10",
  "outbound_delay_blocks": 136,
  "outbound_delay_seconds": 2040,
  "slippage_bps": 41
}
```

Notice how `thorname:10` has been appended to the end of the memo. This instructs THORChain to skim 10 basis points from the swap. The user should still expect to receive the _expected\_amount\_out,_ meaning the affiliate fee has already been subtracted from this number.

For more information on affiliate fees: [Fees.](../../../../maya-protocol-devs/mayachain/concepts/fees.md)

### Error Handling

The quote swap endpoint simulates all of the logic of an actual swap transaction. It ships with comprehensive error handling.

<figure><img src="https://608239768-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MMDO05XcoLd_NnT69p8%2Fuploads%2FbqFMoVMqYOtP2fiT4etE%2Fimage.png?alt=media&#x26;token=638a7fae-4704-4c61-b847-9955742de2e0" alt=""><figcaption><p>This error means the swap cannot be completed given your price tolerance.</p></figcaption></figure>

<figure><img src="https://608239768-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MMDO05XcoLd_NnT69p8%2Fuploads%2FpvdYgagOek2jta3w3Bu7%2Fimage.png?alt=media&#x26;token=5107a0e7-05ce-4d63-826e-66cea726d5e9" alt=""><figcaption><p>This error ensures the destination address is for the chain specified by to_asset.</p></figcaption></figure>

<figure><img src="https://608239768-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MMDO05XcoLd_NnT69p8%2Fuploads%2FrRaIpe55Cc6JlFwMiwZS%2Fimage.png?alt=media&#x26;token=58313b9c-a7c9-48c5-8d83-d144903573f5" alt=""><figcaption><p>This error is due to the fact the affiliate address is too long given the source chain's memo length requirements. Try registering a THORName to shorten the memo.</p></figcaption></figure>

<figure><img src="https://608239768-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MMDO05XcoLd_NnT69p8%2Fuploads%2FRYFfKEDUhLp79svG3363%2Fimage.png?alt=media&#x26;token=ae4bb71b-1f3c-4e7a-989e-0f1a790defee" alt=""><figcaption><p>This error means the requested asset does not exist.</p></figcaption></figure>

<figure><img src="https://608239768-files.gitbook.io/~/files/v0/b/gitbook-x-prod.appspot.com/o/spaces%2F-MMDO05XcoLd_NnT69p8%2Fuploads%2F5r8QqDmCJzIcXXkkJ6Az%2Fimage.png?alt=media&#x26;token=8b98c27e-a2fc-4e24-aa06-912bd374c5ea" alt=""><figcaption><p>Bound checks are made on both affiliate_<em>bps</em> and <em></em> tolerance_bps.</p></figcaption></figure>

### Support

Developers experiencing issues with these APIs can go to the [Developer Discord](http://discord.gg/mayaprotocol) for assistance. Interface developers should subscribe to the #interface-alerts channel for information pertinent to the endpoints and functionality discussed here.
