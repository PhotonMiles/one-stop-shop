---
description: Swap Fees and Delay
---

# Fees and Wait Times

## Fee Types

Users pay up to four two kinds of fees when conducting a swap.

1. **Layer1 Network Fees** (gas): paid by the user when sending the asset to MAYAChain to be swapped. This is controlled by the user's wallet.
2. **Slip Fee**: protects the pool from being manipulated by large swaps. Calculated as a function of transaction size and current pool depth. The slip fee formula is explained [here](https://docs.mayachain.org/mayachain-finance/continuous-liquidity-pools#clp-derivation) and an example implementation is [here](https://gitlab.com/mayachain/asgardex-common/asgardex-util/-/blob/master/src/calc/swap.ts#L57).
3. **Affiliate Fee** - (optional) a percentage skimmed from the inbound amount that can be paid to exchanges or wallet providers.&#x20;
4. **Outbound Fee** - the fee the Network pays on behalf of the user to send the outbound transaction. Usually \~3x the inbound fee.

{% hint style="info" %}
The Swap Quote endpoint will calculate and show all fees.&#x20;
{% endhint %}

See the [fees ](../concepts/fees.md)section for full details.

### Refunds and Minimum Swap Amount

If a transaction fails, it is refunded, thus it will pay the `outboundFee` for the **SourceChain** not the DestinationChain. Thus devs should always swap an amount that is a maximum of, (plus an added buffer to account for potential gas spikes):

1. The Destination Chain outboundFee, or
2. The Source Chain outboundFee, or
3. $1.00 (the minimum outboundFee).

The outbound fee for each chain is returned on the [Inbound Addresses](https://mayanode.mayachain.info/mayachain/inbound\_addresses) endpoint, priced in the gas asset of that chain.&#x20;

So, the steps to determine the minimum swappable amount for a pair:

1. Retrieve the outbound\_fee for the source chain and destination chain from the [Inbound Addresses endpoint](https://mayanode.mayachain.info/mayachain/inbound\_addresses)
2. Convert both outbound\_fees to the input asset of the swap&#x20;

## Wait Times

There are four phases of a transaction sent to MAYAChain each taking time to complete.

1. **Layer1 Inbound Confirmation -** assuming the inboundTx will be confirmed in the next block, it is the source blockchain block time.&#x20;
2. **Observation Counting** - time for 67% MAYAChain Nodes to observe and agree on the inboundTx.&#x20;
3. **Confirmation Counting** - for non-instant finality blockchains, the amount of time MAYAChain will wait before processing to protect against double spends and re-org attacks.
4. **Outbound Delay** - dependent on size and network traffic. Large outbounds will be delayed.&#x20;
5. **Layer1 Outbound Confirmation** - Outbound blockchain block time.

Wait times can be between a few seconds up to an hour. The assets being swapped, the size of the swap and the current network traffic within MAYAChain will determine the wait time.

{% hint style="info" %}
The Swap Quote endpoint will calculate points 3 and 4.
{% endhint %}

See the [Delays ](../concepts/delays.md)section for full details.&#x20;
