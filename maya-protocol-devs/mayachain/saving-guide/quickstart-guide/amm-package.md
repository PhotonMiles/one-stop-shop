---
description: API for performing actions with MAYAChain
---

# AMM Package

While the Query package allows quick and powerful information retrieval from MAYAChain such as a swap estimate., this package performs the actions (sends the transactions), such as a swap, add and remove.&#x20;

As a general rule, this package should be used in conjunction with the query package to first check if an action is going to be possible be performing the action.&#x20;

Example: call estimateSwap first to see if the swap is going to be successful before calling doSwap, as doSwap will not check.&#x20;

#### Code examples in Replit

Currently implemented functions are listed below with code examples. Press the Run button to run the code and see the output. Press,`Show Files`, and select `index.ts` to see the code. Select `package.json` to see all the package dependencies. [Github link](https://github.com/xchainjs/xchainjs-lib/tree/master/packages/xchain-mayachain-amm) and [install instructions](../../../../mayanodes/maya-protocol-devs/mayachain/examples/typescript-web/overview.md).&#x20;

### DoSwap

Executes a swap from a given wallet. This will result in the inbound transaction into MAYAChain.&#x20;

DoSwap runs [EstimateSwap ](../query-package.md)first then if successful [sends a transaction](../../concepts/sending-transactions.md) with a constructed [transaction memo ](../../concepts/transaction-memos.md)using a [chain client](../fees-and-wait-times/client-packages.md). Do swap can be used with an existing xchain client implementation or a custom wallet and will return the transaction hash of the inbound transaction.&#x20;

A seed is provided in the example but it has no funds so it will error.&#x20;

{% embed url="https://replit.com/@mayachain/doSwap-Single#index.ts" %}

### Savers

Adds and removed liquidity from Savers. Requires a seed with funds.&#x20;

{% embed url="https://replit.com/@mayachain/saversTs#index.ts" %}

### Add Liquidity

Adds liquidity to a pool. Provide both assets for the pool. lp type is determined from the amount of the asset. The example is a single-sided cacao deposit. A seed is provided in the example but it has no funds so it will error.&#x20;

{% embed url="https://replit.com/@mayachain/addLiquidity#index.ts" %}

### Remove Liquidity

Removes Liquidity from a pool. The opposite of adding liquidity. &#x20;

{% embed url="https://replit.com/@mayachain/withdrawLiquidity" %}

&#x20;
