# Packages Breakdown

### How xChainjs is constructed

### xchain-\[chain] clients

Each blockchian that is integrated into MAYAChain has a corresponding xchain client with a suite of functionality to work with that chain. They all extend the `xchain-client` class.

### xchain-mayachain

This package is built from OpenAPI-generator and is also used by the mayachain-query. The design is similar to the midgard. Mayanode should only be used when time-sensitive data is required else midgard should be used.&#x20;
