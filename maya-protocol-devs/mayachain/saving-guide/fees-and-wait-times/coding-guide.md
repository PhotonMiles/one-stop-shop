# Coding Guide

A coding overview to xchainjs.

### **General**

The foundation of xchainjs is defined in the [xchain-util](https://github.com/xchainjs/xchainjs-lib/tree/master/packages/xchain-util) package

* `Address`: a crypto address as a string.
* `BaseAmount`:  a bigNumber in 1e8 format. E.g. 1 BTC = 100,000,000 in BaseAmount
* `AssetAmount`: a BaseAmount\*10^8. E.g. 1 BTC = 1 in Asset Amount.&#x20;
* `Asset`: Asset details {Chain, symbol, ticker, isSynth}

{% hint style="info" %}
All `Assets` must conform to the [Asset Notation](../../concepts/transaction-memos.md)

`assetFromString()` is used to quickly create assets and will assign chain and synth.
{% endhint %}

* `CryptoAmount:` is a class that has:

```
 baseAmount: BaseAmount
 readonly asset: Asset
```

All crypto should use the `CryptoAmount` object with the understanding they are in BaseAmount format. An example to switch between them:

```typescript
// Define 1 BTC as CryptoAmount
oneBtc = new CryptoAmount(assetToBase(assetAmount(1)), assetFromStringEx(`BTC.BTC`))
// Print 1 BTC in BaseAmount 
console.log(oneBtc.amount().toNumber().toFixed()) // 100000000
// Print 1 BTC in Asset Amount 
console.log(oneBtc.AssetAmount().amount().toNumber().toFixed()) // 1
```

## Query

Major data types for the mayachain-query package.&#x20;

* [Package description](../query-package.md)
* [Github source code](https://github.com/xchainjs/xchainjs-lib/tree/master/packages/xchain-mayachain-query)
* [Code examples](../query-package.md)
* [Install procedures](../../../../mayanodes/maya-protocol-devs/mayachain/examples/typescript-web/overview.md)

### **EstimateSwapParams**

The input Type for `estimateSwap`. This is designed to be created by interfaces and passed into EstimateSwap. Also see [Swap Memo](../../concepts/transaction-memos.md) for more information.&#x20;

| Variable            | Data Type    | Comments                        |
| ------------------- | ------------ | ------------------------------- |
| input               | CryptoAmount | inbound asset and amount        |
| destinationAsset    | Asset        | outbound asset                  |
| destinationAddress  | String       | outbound asset address          |
| slipLimit           | BigNumber    | Optional: Used to set LIM       |
| affiliateFeePercent | number       | Optional: 0-0.1 allowed         |
| affiliateAddress    | Address      | Optional: thor address          |
| interfaceID         | string       | Optional: assigned interface ID |

### **SwapEstimate**

The internal return type is used within estimateSwap after the calculation is done.&#x20;

| Variable        | Data Type    | Comments                          |
| --------------- | ------------ | --------------------------------- |
| totalFees       | TotalFees    | All fees for swap                 |
| slipPercentage  | BigNumber    | Actual slip of the swap           |
| netOutput       | CryptoAmount | input - totalFees                 |
| waitTimeSeconds | number       | Estimated time for the swap       |
| canSwap         | boolean      | false if there is an issue        |
| errors          | string array | contains info if canSwap is false |

### **TxDetails**

Return type of `estimateSwap`. This is designed to be used by interfaces to give them all the information they need to display to the user.&#x20;

| Variable   | Data Type    | Comments                                                   |
| ---------- | ------------ | ---------------------------------------------------------- |
| txEstimate | SwapEstimate | swap details                                               |
| memo       | string       | constructed memo MAYAChain will understand                 |
| expiry     | DateTime     | when the SwapEstimate information will no longer be valid. |
| toAddress  | string       | current Asgard Vault address From inbound\_address         |

{% hint style="danger" %}
Do not use `toAddress` after `expiry` as the Asgard vault rotates
{% endhint %}

## AMM

Major data types for the mayachain-query package.&#x20;

* [Package description](../quickstart-guide/amm-package.md)
* [Github source code](https://github.com/xchainjs/xchainjs-lib/tree/master/packages/xchain-mayachain-amm)
* [Code examples](../quickstart-guide/amm-package.md)
* [Install procedures](../../../../mayanodes/maya-protocol-devs/mayachain/examples/typescript-web/overview.md)

### **ExecuteSwap**

Input Type for doSwap where a swap will be actually conducted. Based on EstimateSwapParams.

### **TxSubmitted**

| Variable        |        |                             |
| --------------- | ------ | --------------------------- |
| hash            | string | inbound Tx Hash             |
| url             | string | Block exploer url           |
| waitTimeSeconds | number | Estimated time for the swap |
