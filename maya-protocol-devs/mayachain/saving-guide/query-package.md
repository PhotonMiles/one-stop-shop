---
description: How to query MAYAChain information
---

# Query Package

This package is designed to obtain any desired information from MAYAChain that a developer could want. While it uses Midgard and Mayanode packages it is much more powerful as it knows where to get the best information and how to package the information for easy consumption by developers or higher functions.&#x20;

It exposes several simple functions that implement all of MAYAChain's complexity to allow easy information retrieval. The Query package does not perform any actions on MAYAChain or send transactions, that is the job of the [Thorchain-AMM package](quickstart-guide/amm-package.md).&#x20;

#### Code examples in Replit

Currently implemented functions are listed below with code examples. Press the Run button to run the code and see the output. Press Show files, and select index.ts to see the code. Select package.json to see all the package dependencies. [Repo link](https://github.com/xchainjs/xchainjs-lib/tree/master/packages/xchain-mayachain-query) and [install instructions](../../../mayanodes/maya-protocol-devs/mayachain/examples/typescript-web/overview.md).&#x20;

### Estimate Swap

Provides estimated swap information for a single or double swap for any given two assets within MAYAChain. Designed to be used by interfaces, see more info [here](fees-and-wait-times/coding-guide.md). EstimateSwap will do the following:

* Validate swap inputs
* Checks for [network or chain halts](../concepts/network-halts.md)
* Get the latest pool data from [Midgard](../concepts/connecting-to-mayachain.md)
* Work out the swap [slip](../concepts/math.md), swap [fee ](../../../maya-protocol-docs/how-it-works/fees.md)and [output](../concepts/math.md)
* Deducts all [fees ](../../../maya-protocol-docs/how-it-works/fees.md)from the input amount (inbound, swap, outbound and any affiliate) in the correct order to produce `netOutput` and detail fees in `totalFees`
* Ensures `totalFees` is not greater than `input`.
* Work out the expected [wait time](../concepts/delays.md) including confirmation counting and outbound delay.&#x20;
* Get the current [Asgard Vault address](../concepts/querying-mayachain.md) for the inbound asset
* Advise if a swap is possible and provide a reason if it is not.&#x20;

Note: This will be the best estimate using the information available, however exact values will be different depending on pool depth changes and network congestion.&#x20;

{% embed url="https://replit.com/@mayachain/estimateSwap#index.ts" %}

### Savers

Shows use of the savers quote endpoints.

{% embed url="https://replit.com/@mayachain/quoteSaversTS#index.ts" %}

### Check Balance

Checks the liquidity position of a given address in a given pool. Retrieves information such as current value, ILP coverage, pool share and last added.

{% embed url="https://replit.com/@mayachain/checkLiquidity#package.json" %}

### Check Transaction

Provide the status of a transaction given an input hash (e.g. the output of doSwap). Looks at the different stages a transaction can be in and report.&#x20;

In development

### Estimate Add Liquidity

Provides an estimate for given add liquidity parameters such as slip fee, transaction fees, and expected wait time. Supports symmetrical, asymmetrical and uneven additions.&#x20;

{% embed url="https://replit.com/@mayachain/estimateWithdrawLiquidity#index.ts" %}

### Estimate Remove Liquidity

Provides information for a planned withdrawal for a given liquidity position and withdrawal percentage. Information such as fees, wait time and ILP coverage

{% embed url="https://replit.com/@mayachain/estimateWithdrawLiquidity#index.ts" %}

### List Pools

Lists all the pool detail within MAYAChain.

{% embed url="https://replit.com/@mayachain/listPools#index.ts" %}

### Network Values

List current network values from constants and mimir. If mimir override exists, it is displayed instead.&#x20;

{% embed url="https://replit.com/@mayachain/networkSetting#index.ts" %}

If there is a function you want to be added, reach out in Telegram or the dev discord server.&#x20;
