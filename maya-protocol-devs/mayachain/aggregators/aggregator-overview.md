---
description: How MAYAChain supports cross-chain aggregation.
---

# Aggregator Overview

## Overview

MAYAChain will only support a set number of assets and is not designed to support log tailed assets. If a user wants to swap from a long tail ERC20 asset to Bitcoin, they have to use an Ethereum AMM like Sushi Swap to swap the ERC20 asset to ETH then they can swap the ETH to BTC.&#x20;

The same process applies for long tail tokens on other chains such as Avalance and Cosmos.&#x20;

**Aggregator** is the ability for a user swap long tail assets via leveraging a supported on-chain AMMs and MAYAChain in one transaction.&#x20;

To support cross-chain aggregation, MAYAChain whitelists [aggregator contracts](https://gitlab.com/mayachain/mayanode/-/blob/develop/x/mayachain/aggregators/dex\_mainnet.go) that can call into MAYAChain (**Swap In)**, or receive calls (**Swap Out**). Chains that do not have on-chain AMMs (like Bitcoin) cannot support **SwapIn**, but they can support **SwapOut**, since they can pass a memo to MAYAChain.&#x20;

ETH swap contracts such as Sushi Swap to convert to/from MAYAChain support L1 tokens such as BTC.  Example, in one transaction:

1. User swaps long-tail ERC20 to ETH in SushiSwap, then swaps that ETH to BTC.
2. User swaps BTC into ETH, then swaps that ETH into long-tail ERC20

There can be multiple `aggregators`. The first `mayachain aggregator` will use Sushiswap only and use ETH as the base asset. Aggregators need to follow a spec for compatibility with MAYAChain. Any MAYAChain ecosystem project can launch their own aggregator and get it whitelisted into MAYAChain. They can add custom/exotic routing logic if they wish.

#### SwapIn

The SwapIn is called by the User, which then passes a memo to MAYAChain to do the final swap.&#x20;

`User -> Call Into Aggregator -> Swap Via AMM -> Deposit into MAYAChain -> Swap to Base Asset`

Eg: Swap long tail ERC20 via Sushiswap into BTC on MAYAChain.&#x20;

[Transaction Example](https://etherscan.io/tx/0x7905c41daaa214fbb3bad79ef63bb69aafcb15147f53cd9cf621d4049c2cea4d) using [UniSwap](https://etherscan.io/address/0x86904eb2b3c743400d03f929f2246efa80b91215) to swap ETH.ENJ to BNB.BNB.

#### SwapOut

The SwapOut is called by the User invoking the aggregator memo on MAYAChain.&#x20;

The User needs to pass the aggregator contract address in the memo. MAYAChain will perform the swap to the preferred Base Asset for that chain. The rest of the parameters, being `to, asset, limit` are what is passed by MAYAChain in the SwapOut call for further execution.

`User ->  Deposit into MAYAChain -> Swap to Base Asset -> Call into Aggregator -> Swap Via AMM`

Eg: Swap from BTC on MAYAChain to long tail ERC20 via Sushiswap. See [Memos](memos.md).&#x20;

#### Combined

A user can combine the two. Swapping In first, then passing an Aggregator Memo to MAYAChain. This will cause MAYAChain to perform a **SwapOut**.

`User -> Swap In -> MAYAChain -> Swap Out`

Eg: Swap long tail ERC20 via Sushiswap into ETH on MAYAChain to LUNA then long tail CW20 via TerraSwap.

### EVM Implementation

{% content-ref url="evm-implementation.md" %}
[evm-implementation.md](evm-implementation.md)
{% endcontent-ref %}

### CosmWasm Implementation

For **SwapIn** The caller must first execute a `MsgExecuteContract`, then call a `MsgSend` into MAYAChain vaults with the correct memo.&#x20;

For **SwapOut** MAYAChain will execute a `MsgExecuteContract` which then sends the final asset to the user. If failed, MAYAChain will execute the fallback and send the member the base asset instead.

### Deploying An Aggregator

If you would like to deploy your own aggregator with your own custom logic, deploy it with the principles above, then submit a PR for it to get whitelisted on MAYAChain.&#x20;

{% embed url="https://gitlab.com/mayachain/mayanode/-/merge_requests/2132/diffs" %}
