---
description: Overview of MAYAChain and links to frontend guides.
---

# Introduction

Maya Protocol is the 2nd multi-chain DEX in the market: a decentralized liquidity protocol forked from THORChain that coordinates asset movement, including processing swaps, stakes, and more, without the need to peg or wrap assets. Additionally, it is open-source, so it is community-owned and protected by code you can audit.

The protocol observes incoming user deposits to vaults, executes business logic (swap, add/remove liquidity), and processes outbound transactions. Maya Protocol is primarily a leaderless vault manager, ensuring that every process stage is byzantine-fault-tolerant.

The key objective is to resist centralization and capture whilst facilitating cross-chain liquidity. Maya Protocol only secures the assets in its vaults and has economic guarantees that those assets are safe.

Each MAYANode is comprised of several independent servers in a cluster, which run full-nodes for each connected chain, a MayaDaemon, and a [Luum](https://gitlab.com/mayachain/docs/one-stop-shop/-/tree/main) API. MayaNodes should be anonymous, do not support delegation, and are regularly churned out.

## $CACAO

CACAO is the asset that powers Maya Protocol and provides the economic incentives required to secure the network. CACAO has four key roles, which are described below.

1. Liquidity (as a settlement asset)
2. Security (as a Sybil-resistant mechanism and a means for driving economic behavior)
3. Governance (signaling priority on-chain)
4. Incentives (paying out rewards, charging fees, subsidizing gas)

## ROLES

There are four key roles in the system:

1. **Liquidity providers** add liquidity to pools and earn fees and rewards
2. **Swappers** use the liquidity to swap assets ad-hoc, paying fees
3. **Traders** monitor pools and rebalance continually, paying fees but intending to earn a profit.
4. [**Liquidity Nodes**](https://docs.mayaprotocol.com/readme-1/liquidity-nodes) provide a bond (in $CACAO and LP units) and are paid to secure the system.

## MayaNodes

MayaNodes service the Maya Protocol network, which is intended to be initially 100 but can scale to 300. The design goal of Maya is such that anyone can join the network with the required funds (permissionless) \*and be anonymous\*, yet still, be secure. Maya takes this a step further by having a VERY high churn schedule, kicking out nodes continuously. This high-churn network ensures that it is censorship-resistant, evades capture, and resists centralization.

Each MAYANode comprises several independent servers in a cluster, which run full nodes for each connected chain, a MayaDaemon, and a [Luum](https://gitlab.com/mayachain/docs/one-stop-shop/-/tree/main) API. MayaNodes should be anonymous, do not support delegation, and are regularly churned out.

## Developers

{% hint style="info" %}
Users make signed transactions with a "memo" conveying intent into vaults, which is then picked up by MAYAChain and executed.\
They do not need to hold CACAO, or even care that CACAO was used, or even connect directly with MAYAChain. This makes MAYAChain suitable to be integrated into almost any wallet/service/exchange provider as the "backend" liquidity protocol.
{% endhint %}

{% hint style="danger" %}
MAYAChain churns vaults regularly to resist capture. Do not send transactions directly to MAYAChain vaults without first doing important safety checks.
{% endhint %}

## Interfaces

Interfaces allow users to connect to wallets, read balances, query [Luum](https://gitlab.com/mayachain/docs/one-stop-shop/-/tree/main), and broadcast transactions in both the web and desktop environment. Anyone can build their own interface, and several wallet libraries have been built to help developers with this.

### API - Midgard

Luum is run by every MayaNode and provides a restful API & graphQL & WebSockets that any client can consume to display data. To connect to [Luum](broken-reference/), the client (e.g. wallet) must first challenge a number of nodes to prevent being attacked or phished since the security model of Maya is strictly by consensus. Or the interface team can run their own Luum.

### State Machine - Maya Protocol

Maya itself is a state machine that both settles the external state and transactions of MAYA.CACAO - the network asset. Maya could be called an app chain, where the application is a decentralized exchange focused on liquidity.

## Integration

Developers build products that integrate with Maya, such as wallets, exchanges, and other services. Developers simply need to connect to [Luum](https://gitlab.com/mayachain/docs/one-stop-shop/-/tree/main), they do not need to run a node.

The order of integration is as follows:

1. Connect to Maya Protocol via [Luum](https://gitlab.com/mayachain/docs/one-stop-shop/-/tree/main)
2. Use data provided by [Luum](https://gitlab.com/mayachain/docs/one-stop-shop/-/tree/main) to display (pools, assets)
3. Use `xchain-crypto` to manage key stores for your wallet
4. Use `xchainjs` packages to sign transactions and broadcast.

## Contributing

Maya Protocol is a public project. Anyone can help contribute to the ecosystem. Start here to learn about the contribution process, as well as upgrading the chain:

1. Connecting to more chains
2. Adding more business logic to the chain (stablecoins, derivatives, etc.)
3. Upgrading the chain to be more robust
