---
description: Transaction Memo Details
---

# Transaction Memos

### Overview

Transactions to MAYAChain pass user-intent with the `MEMO` field on their respective chains. MAYAChain inspects the transaction object as well as the `MEMO` in order to process the transaction, so care must be taken to ensure the `MEMO` and the transaction are both valid. If not, MAYAChain will automatically refund the assets. All valid memos are listed [here](https://gitlab.com/mayachain/mayanode/-/blob/develop/x/mayachain/memo/memo.go#L41).

Different chains have different ways of adding state to a transaction. Long assets can be shortened using Asset abbreviations (below) as well as MAYANames to reduce the size of destination/affiliate addresses.

### Format

Memos follow the format:

`FUNCTION:PARAM1:PARAM2:PARAM3:PARAM4`

The function is invoked by a string, which in turn calls a particular handler in the state machine. The state machine parses the memo looking for the parameters which is simply decodes from human-readable strings.&#x20;

In addition, some parameters are optional. Simply leave them blank, but retain the `:` separator:

`FUNCTION:PARAM1:::PARAM4`

### Permitted Memos

The following memos are permitted:

1. **SWAP**
2. **Deposit** Savers
3. **Withdraw** Savers
4. **ADD** Liquidity
5. **WITHDRAW** Liquidity
6. **BOND**, **UNBOND** & **LEAVE**
7. **DONATE** & **RESERVE**
8. **NOOP**

### Swap

Perform a swap.&#x20;

**`SWAP:ASSET:DESTADDR:LIM:AFFILIATE:FEE`**

| Parameter    | Notes                                                                              | Conditions                                  |
| ------------ | ---------------------------------------------------------------------------------- | ------------------------------------------- |
| Payload      | Send the asset to swap.                                                            | Must be an active pool on MAYAChain.        |
| `SWAP`       | The swap handler                                                                   | Also `s`, `=`                               |
| `:ASSET`     | The asset identifier.                                                              | Can be shortened.                           |
| `:DESTADDR`  | The destination address to send to.                                                | Can use MAYAName.                           |
| `:LIM`       | The trade limit ie, set 100000000 to get a minimum of 1 full asset, else a refund. | Optional, 1e8 format                        |
| `:AFFILIATE` | The affiliate address. CACAO is sent to Affiliate.                                  | Optional. Must be MAYAName or MAYA Address. |
| `:FEE`       | The affiliate fee. Limited from 0 to 1000 Basis Points.                            | Optional                                    |

**Examples**

**`SWAP:ASSET:DESTADDR`** simply swap

**`=:ASSET:DESTADDR:LIM`** swap with limit

**`s:ASSET:DESTADDR:LIM:AFFILIATE:FEE`** swap with limit and affiliate

`=:MAYA.CACAO:maya1el4ufmhll3yw7zxzszvfakrk66j7fx0tvcslym:19779138111`

`s:BNB/BUSD-BD1:maya15s4apx9ap7lazpsct42nmvf0t6am4r3w0r64f2:628197586176`&#x20;

### Deposit Savers <a href="#swap" id="swap"></a>

Depositing savers can work without a memo however memos are recommended to be explicit about the transaction intent.&#x20;

| Paramater | Notes                             | Conditions                     |
| --------- | --------------------------------- | ------------------------------ |
| Payload   | The asset to add liquidity with.  | Must be supported by MAYAChain |
| ADD       | The Deposit handler.              | Also `a` `+`                   |
| `:POOL`   | The pool to add liquidity to.     | Gas pools only.                |

**Examples**\
`+:BTC.BTC` add to the BTC Savings Vault

`a:ETH.ETH` add to the ETH Savings Vault

{% hint style="info" %}
An affiliate option for savers is being developed.
{% endhint %}

### **Withdraw Savers** <a href="#swap" id="swap"></a>

| Paramater      | Notes                                                                                                                                                         | Extra                                                                                                                                                      |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Payload        | Send the [dust threshold](https://midgard.mayachain.info/v2/mayachain/inbound\_addresses) of the asset to cause the transaction to be picked up by MAYAChain. | Caution [Dust Limits](https://midgard.mayachain.info/v2/mayachain/inbound\_addresses): BTC,BCH,LTC chains 10k sats; DOGE 1m Sats; ETH 0 wei; MAYA 0 CACAO.  |
| `WITHDRAW`     | The withdraw handler.                                                                                                                                         | Also `-` `wd`                                                                                                                                              |
| `:POOL`        | The pool to withdraw liquidity from.                                                                                                                          | Gas pools only.                                                                                                                                            |
| `:BASISPOINTS` | Basis points (0-10000, where 10000=100%)                                                                                                                      |                                                                                                                                                            |

**Examples**

`-:BTC.BTC:10000` Withdraw 100% from BTC Savers

`w:ETH.ETH:5000` Withdraw 50% from ETH Savers

### Add Liquidity <a href="#swap" id="swap"></a>

There are rules for adding liquidity, see [the rules here](https://docs.mayachain.org/learn/getting-started#entering-and-leaving-a-pool) and regardless of how it is added, it is subject to [Impermanent Loss Protection](https://docs.mayachain.org/learn/understanding-mayachain#ilp-how-mayachain-protects-its-liquidity-providers). Also see [Impermanent Loss Protection Details](https://docs.mayachain.org/mayachain-finance/continuous-liquidity-pools#impermanent-loss-protection). \
**`ADD:POOL:PAIREDADDR:AFFILIATE:FEE`**

| Parameter     | Notes                                                                                                                                                                                                                                   | Conditions                                                                   |
| ------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| Payload       | The asset to add liquidity with.                                                                                                                                                                                                        | Must be supported by MAYAChain.                                              |
| `ADD`         | The Add Liquidity handler.                                                                                                                                                                                                              | also `a` `+`                                                                 |
| `:POOL`       | The pool to add liquidity to.                                                                                                                                                                                                           | Can be shortened.                                                            |
| `:PAIREDADDR` | The other address to link with. If on external chain, link to MAYA address. If on MAYAChain, link to external address. If a paired address is found, the LP is matched and added. If none is found, the liquidity is put into pending.  | Optional. If not specified, a single-sided add-liquidity action is created.  |
| `:AFFILIATE`  | The affiliate address. The affiliate is added in to the pool as an LP.                                                                                                                                                                  | Optional. Must be MAYAName or MAYA Address.                                  |
| `:FEE`        | The affiliate fee. Fee is allocated to the affiliate.                                                                                                                                                                                   | Optional. Limited from 0 to 1000 Basis Points.                               |
| `:TIER`       | Only available when the [Liquidity Auction](https://docs.mayaprotocol.com/readme/fair-launch/technical-overview-fl) is enabled. This sets the tier in which you would like your liquidity to be in.                                     | Optional. TIER1, TIER2 or TIER3 (Default value is TIER3 if omitted)          |

**Examples**

**`ADD:POOL`** Single-sided add liquidity.  If this is a position's first add, liquidity can only be withdrawn to the same address.

**`+:POOL:PAIREDADDR`** Add on both sides.&#x20;

**`a:POOL:PAIREDADDR:AFFILIATE:FEE`** Add with affiliate

**`+:BTC.BTC`** Add single-sided BTC

**`+:POOL:PAIREDADDR:TIER`**  Add with tier

**`+:POOL:PAIREDADDR:AFFILIATE:FEE:TIER` ** Add with affiliate and tier

### Withdraw Liquidity

Withdraw liquidity from a pool.\
A withdrawal can be either dual-sided (withdrawn based on pool's price) or entirely single-sided (converted to one side and sent out).

**`WITHDRAW:POOL:BASISPOINTS:ASSET`**

| Parameter      | Notes                                                                                                    | Extra                                                                                                                                                      |
| -------------- | -------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Payload        | Send the dust threshold of the asset to cause the transaction to be picked up by MAYAChain.              | Caution [Dust Limits](https://midgard.mayachain.info/v2/mayachain/inbound\_addresses): BTC,BCH,LTC chains 10k sats; DOGE 1m Sats; ETH 0 wei; MAYA 0 CACAO.  |
| `WITHDRAW`     | The withdraw handler.                                                                                    | Also `-` `wd`                                                                                                                                              |
| `:POOL`        | The pool to withdraw liquidity from.                                                                     | Can be shortened.                                                                                                                                          |
| `:BASISPOINTS` | Basis points (0-10000, where 10000=100%)                                                                 |                                                                                                                                                            |
| `:ASSET`       | Single-sided withdraw to one side.                                                                       | Optional. Can be shortened. Must be either CACAO or the ASSET.                                                                                              |
| `:PAIREADDR`   | Only useful when trying to withdraw from external chain. This should be the MAYA address you paired with | Optional                                                                                                                                                   |

**Examples**

**`WITHDRAW:POOL:10000`** dual-sided 100% withdraw liquidity.  If a single-address position, this withdraws single-sidedly instead.

**`-:POOL:1000`** dual-sided 10% withdraw liquidity.&#x20;

**`wd:POOL:5000:ASSET`** withdraw 50% liquidity as the asset specified while the rest stays in the pool, eg:

`wd:BTC.BTC:5000:BTC.BTC`

### DONATE & RESERVE

Donate to a pool or the RESERVE.

**`DONATE:POOL`**

| Parameter | Notes                                    | Extra                                                 |
| --------- | ---------------------------------------- | ----------------------------------------------------- |
| Payload   | The asset to donate to a MAYAChain pool. | Must be supported by MAYAChain. Can be CACAO or ASSET. |
| `DONATE`  | The donate handler.                      | Also `%`                                              |
| `:POOL`   | The pool to withdraw liquidity from.     | Can be shortened.                                     |

**`RESERVE`**

| Parameter | Notes                | Extra                              |
| --------- | -------------------- | ---------------------------------- |
| Payload   | MAYA.CACAO            | The CACAO to credit to the RESERVE. |
| `RESERVE` | The reserve handler. |                                    |

### BOND, UNBOND & LEAVE

Perform node maintenance features.&#x20;

**`BOND:NODEADDR:PROVIDER:FEE`**

| Parameter   | Notes                                    | Extra                                                                                   |
| ----------- | ---------------------------------------- | --------------------------------------------------------------------------------------- |
| Payload     | The asset to bond to a  Node.            | Must be CACAO.                                                                           |
| `BOND`      | The bond handler.                        | Anytime.                                                                                |
| `:NODEADDR` | The node to bond with.                   |                                                                                         |
| `:PROVIDER` | Whitelist in a provider.                 | Optional, add a provider                                                                |
| `:FEE`      | Specify an Operator Fee in Basis Points. | Optional, default will be the mimir value (2000 Basis Points). Can be changed anytime.  |

**`UNBOND:NODEADDR:AMOUNT`**

| Parameter   | Notes                    | Extra                    |
| ----------- | ------------------------ | ------------------------ |
| Payload     | None required            | Use `MsgDeposit`         |
| `UNBOND`    | The unbond handler.      |                          |
| `:NODEADDR` | The node to unbond from. | Must be in standby only. |

**`LEAVE:NODEADDR`**

| Parameter   | Notes                       | Extra                                                                                                     |
| ----------- | --------------------------- | --------------------------------------------------------------------------------------------------------- |
| Payload     | None required               | Use `MsgDeposit`                                                                                          |
| `LEAVE`     | The leave handler.          |                                                                                                           |
| `:NODEADDR` | The node to force to leave. | If in Active, request a churn out to Standby for 1 churn cycle. If in Standby, forces a permanent leave.  |

**Examples**

`BOND:maya1xd4j3gk9frpxh8r22runntnqy34lwzrdkazldh`

`LEAVE:maya18r8gnfm4qjak47qvpjdtw66ehsx49w99c5wewd`

### NOOP

Dev-centric functions to fix MAYAChain state. Caution: may cause loss of funds if not done exactly right at the right time.&#x20;

**`NOOP`**

| Parameter  | Notes                           | Extra                                                    |
| ---------- | ------------------------------- | -------------------------------------------------------- |
| Payload    | The asset to credit to a vault. | Must be ASSET or CACAO.                                   |
| `NOOP`     | The  noop handler.              | Adds to the vault balance, but does not add to the pool. |
| `:NOVAULT` | Do not credit the vault.        | Optional. Just fix the insolvency issue.                 |

### Refunds

The following are the conditions for refunds:

| Condition                | Notes                                                                                                        |
| ------------------------ | ------------------------------------------------------------------------------------------------------------ |
| Invalid `MEMO`           | If the `MEMO` is incorrect the user will be refunded.                                                        |
| Invalid Assets           | If the asset for the transaction is incorrect (adding an asset into a wrong pool) the user will be refunded. |
| Invalid Transaction Type | If the user is performing a multi-send vs a send for a particular transaction, they are refunded.            |
| Exceeding Price Limit    | If the final value achieved in a trade differs to expected, they are refunded.                               |

Refunds cost fees to prevent Denial of Service attacks. The user will pay the correct outbound fee for that chain.

### Asset Notation

The following is the notation for Assets in MAYAChain's system:

#### Note: CHAIN.ASSET denotes native asset. CHAIN/ASSET denotes a Synthetic Asset

#### Examples

| Asset         | Notation                                            |
| ------------- | --------------------------------------------------- |
| Bitcoin       | BTC.BTC (Native)                                    |
| Bitcoin       | BTC/BTC (Synth)                                     |
| Ethereum      | ETH.ETH                                             |
| USDT          | ETH.USDT-0xdac17f958d2ee523a2206206994597c13d831ec7 |
| BNB           | BNB.BNB (Native)                                    |
| BNB           | BNB/BNB (Synth)                                     |
| CACAO (BEP2)   | BNB.CACAO-B1A                                        |
| CACAO (NATIVE) | MAYA.CACAO                                           |

### Asset Abbreviations

Assets can be abbreviated using fuzzy logic. The following will all be matched appropriately. If there are conflicts then the deepest pool is matched. (To prevent attacks).

| Notation                                            |
| --------------------------------------------------- |
| ETH.USDT                                            |
| ETH.USDT-ec7                                        |
| ETH.USDT-6994597c13d831ec7                          |
| ETH.USDT-0xdac17f958d2ee523a2206206994597c13d831ec7 |

### Mechanism for Transaction Intent <a href="#mechanism-for-transaction-intent" id="mechanism-for-transaction-intent"></a>

| Chain         | Mechanism            | Notes                                                                      |
| ------------- | -------------------- | -------------------------------------------------------------------------- |
| Bitcoin       | OP\_RETURN           | Limited to 80 bytes.                                                       |
| Ethereum      | Smart Contract Input | Use `deposit(vault, asset, amount, memo)` function, where `memo` is string |
| Binance Chain | MEMO                 | Each transaction has an optional memo, limited to 128 bytes.               |
| Monero        | Extra Data           | Each transaction can have attached `extra data` field, that has no limits. |

Each chain will have a unique way of adding state to a transaction. Long assets can be shortened using Asset abbreviations (below) as well as MAYANames to reduce the size of destination/affiliate addresses.​
