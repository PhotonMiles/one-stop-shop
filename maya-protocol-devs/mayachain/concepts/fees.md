---
description: Understanding how fees are calculated.
---

# Fees

## Overview

There are 4 different fees the user should know about.&#x20;



1. Inbound Fee (sourceChain: gasRate \* txSize)
2. Liquidity Fee (swapSlip \* swapAmount)
3. Affiliate Fee (affiliateFee \* swapAmount)
4. Outbound Fee (destinationChain: gasRate \* txSize)

### **Terms**&#x20;

* **SourceChain**: the chain the user is swapping from&#x20;
* **DestinationChain**: the chain the user is swapping to txSize: the size of the transaction in bytes (or units)&#x20;
* **gasRate**: the current gas rate of the external network&#x20;
* **swapAmount**: the amount the user is swapping swapSlip: the slip created by the&#x20;
* **swapAmount**, as a function of poolDepth&#x20;
* **affiliateFee**: optional fee set by interface in basis points

## Fees Detail

### Inbound Fee

This is the fee the user pays to make a transaction on the source chain, which the user pays directly themselves. The gas rate recommended to use is `fast` where the tx is guaranteed to be committed in the next block. Any longer and the user will be waiting a long time for their swap and their price will be invalid (thus they may get an unnecessary refund).&#x20;

$$
inboundFee = txSize * gasRate
$$

{% hint style="success" %}
`MAYAChain calculates and posts fee rates at` [`https://midgard.mayachain.info/v2/mayachain/inbound_addresses` ](https://midgard.mayachain.info/v2/mayachain/inbound\_addresses)``
{% endhint %}

{% hint style="warning" %}
Always use a "fast" or "fastest" fee, if the transaction is not confirmed in time, it could be abandoned by the network or failed due to old prices. You should allow your users to cancel or re-try with higher fees.
{% endhint %}

### Liquidity Fee

This is simply the slip created by the transaction multiplied by its amount. It is priced and deducted from the destination amount automatically by the protocol.

$$
slip = \frac{swapAmount}{swapAmount + poolDepth}
$$

$$
fee =slip * swapAmount
$$

### Affiliate Fee

In the swap transaction you build for your users you can include an affiliate fee for your exchange (accepted in $CACAO or a synthetic asset, so you will need a $CACAO address).

* The affiliate fee is in basis points (0-10,000) and will be deducted from the inbound swap amount from the user.&#x20;
* If the inbound swap asset is a native MAYAChain asset ($CACAO or synth) the affiliate fee amount will be deducted directly from the transaction amount.&#x20;
* If the inbound swap asset is on any other chain the network will submit a swap to $CACAO with the destination address as your affiliate fee address.&#x20;
* If the affiliate is added to an ADDLP tx, then the affiliate is included in the network as an LP.&#x20;

`SWAP:CHAIN.ASSET:DESTINATION:LIMIT:AFFILIATE:FEE`

Read [https://medium.com/mayachain/affiliate-fees-on-mayachain-17cbc176a11b](https://medium.com/mayachain/affiliate-fees-on-mayachain-17cbc176a11b) for more information.&#x20;

$$
affliateFee = \frac{feeInBasisPoints * swapAmount}{10000}
$$

### Outbound Fee

This is the fee the Network pays on behalf of the user to send the outbound transaction. To adequately pay for network resources (TSS, compute, state storage) the fee is marked up to be 3 times the actual on-chain fee the nodes pay.&#x20;

$$
outboundFee = txSize * gasRate * 3
$$

The minimum Outbound Layer1 Fee the network will charge is on `/mayachain/mimir` and is priced in USD (based on MAYAChain's USD pool prices). This means really cheap chains still pay their fair share. It is currently set to `100000000` = $1.00

See [Outbound Fee ](https://docs.mayachain.org/how-it-works/fees#outbound-fee)for more information.&#x20;

## Fee Ordering for Swaps

Fees are taken in the following order when conducting a swap.

1. Inbound Fee (user wallet controlled, not MAYAChain controlled)
2. Affiliate Fee (if any) - skimmed from the input.
3. Swap Fee (denoted in output asset)
4. Outbound Fee (taken from the swap output)

To work out the total fees, fees should be converted to a common asset (e.g. CACAO or USD) then added up. Total fees should be less than the input else it is likely to result in a refund.

### Refunds and Minimum Swappable Amount

If a transaction fails, it is refunded, thus it will pay the `outboundFee` for the **SourceChain** not the DestinationChain. Thus devs should always swap an amount that is a maximum of the following, plus a buffer to allow for sudden gas spikes:

1. The Destination Chain outbound\_fee
2. The Source Chain outbound\_fee
3. $1.00 (the minimum)

The outbound\_fee for each chain is returned on the [Inbound Addresses](https://mayanode.mayachain.info/mayachain/inbound\_addresses) endpoint, priced in the gas asset.&#x20;

So, to calculate the minimum swappable amount for a swap from **ChainA.AssetA** -> **ChainB.AssetB**:

1. Get the outbound\_fees of both **ChainA** and **ChainB** from the [Inbound Addresses Endpoint](https://mayanode.mayachain.info/mayachain/inbound\_addresses)
2. Convert both outbound\_fees to the swap's input asset (**ChainA.AssetA**) using the [flat exchange rate](math.md) (without slippage)
3. Take the maximum between those two values, and multiply by a healthy buffer to allow room for sudden gas spikes (at least 1.5)

_Remember, if the swap limit is not met or the swap is otherwise refunded the outbound\_fee of the Source Chain will be deducted from the input amount, so give your users enough room._

### Understanding gas\_rate

MAYANode keeps track of current gas prices. Access these at the `/inbound_addresses` endpoint of the [MAYANode API](https://dev.mayachain.org/mayachain-dev/wallets/connecting-to-mayachain#mayanode). The response is an array of objects like this:

```json
{
    "chain": "ETH",
    "pub_key": "mayapub1addwnpepqdlx0avvuax3x9skwcpvmvsvhdtnw6hr5a0398vkcvn9nk2ytpdx5cpp70n",
    "address": "0x74ce1c3556a6d864de82575b36c3d1fb9c303a80",
    "router": "0x3624525075b88B24ecc29CE226b0CEc1fFcB6976",
    "halted": false,
    "gas_rate": "10"
    "gas_rate_units": "satsperbyte",
    "outbound_fee": "30000",
    "outbound_tx_size": "1000",
}
```

The `gas_rate` property can be used to estimate network fees for each chain the swap interacts with. For example, if the swap is `BTC -> ETH` the swap will incur fees on the bitcoin network and Ethereum network. The `gas_rate` property works differently on each chain "type" (e.g. EVM, UTXO, BFT).

The `gas_rate_units` explain what the rate is for chain, as a prompt to the developer.&#x20;

The `outbound_tx_size` is what MAYAChain internally budgets as a typical transaction size for each chain.&#x20;

The `outbound_fee` is `gas_rate * outbound_tx_size * 3` and developers can use this to budget for the fee to be charged to the user.

Keep in mind the `outbound_fee` is priced in the gas asset of each chain. For chains with tokens, be sure to convert the `outbound_fee` to the outbound token to determine how much will be taken from the outbound amount. To do this, use the `getValueOfAsset1InAsset2` formula described in the [`Math`](https://dev.mayachain.org/mayachain-dev/interface-guide/math#example-1) section.

## Fee Calculation by Chain&#x20;

### **MAYAChain (Native Rune)**

The MAYAChain blockchain has a set 0.02 CACAO fee. This is set within the MAYAChain [Constants ](https://mayanode.mayachain.info/mayachain/constants)by `NativeTransactionFee`. As MAYAChain is 1e8, `2000000 TOR = 0.02 CACAO`

### Binance Chain

MAYAChain uses the gas\_rate as the flat Binance Chain transaction fee.

E.g. If the `gas_rate` = 11250 then fee is 0.0011250 BNB.&#x20;

### UTXO Chains like Bitcoin

For UXTO chains link Bitcoin, `gas_rate`is denoted in Satoshis. The `gas_rate` is calculated by looking at the average previous block fee seen by the MAYANodes.

All MAYAChain transactions use BECH32 so a standard tx size of 250 bytes can be used. The standard UTXO fee is then `gas_rate`\* 250.

### EVM Chains like Ethereum

For EVM chains like Ethereum, `gas_rate`is denoted in GWei. The `gas_rate` is calculated by looking at the average previous block fee seen by the MAYANodes

An Ether Tx fee is: `gasRate * 10^9 (GWEI) * 21000 (units).`

An ERC20 Tx is larger: `gasRate * 10^9 (GWEI) * 70000 (units)`

{% hint style="success" %}
`MAYAChain calcuates and posts fee rates at` [`https://midgard.mayachain.info/v2/mayachain/inbound_addresses` ](https://midgard.mayachain.info/v2/mayachain/inbound\_addresses)``
{% endhint %}

