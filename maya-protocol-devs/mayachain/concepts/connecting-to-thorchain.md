---
description: How to connect to Midgard, MAYANode and the base Tendermint layer.
---

# Connecting to MAYAChain

The Network Information comes from four sources:

1. **Midgard**: Consumer information relating to swaps, pools, and volume. Dashboards will primarily interact with Midgard.
2. **MAYANode**: Raw blockchain data provided by the MAYAChain state machine. MAYAChain wallets and block explorers will query MAYAChain-specific information here.
3. **Cosmos RPC**: Used to query for generic CosmosSDK information.
4. **Tendermint RPC**: Used to query for consensus-related information.

{% hint style="info" %}
The below endpoints are run by specific organisations for public use.  There is a cost to running these services. If you want to run your own full node, please see [https://docs.mayachain.org/mayanodes/overview. ](https://docs.mayachain.org/mayanodes/overview)
{% endhint %}

### Midgard

Midgard returns time-series information regarding the MAYAChain network, such as volume, pool information, users, liquidity providers and more. It also proxies to MAYANode to reduce burden on the network.&#x20;

**Runs on every Node**

Port: `8080`

RPC Guide:\
http://\<host>:8080/v2/doc

**Mainnet**

* [https://midgard.mayachain.info/v2/doc](https://midgard.mayachain.info/v2/doc)

**Stagenet**

* [https://stagenet.midgard.mayachain.info/v2/doc](https://stagenet.midgard.mayachain.info/v2/doc)

### MAYANode

MAYANode returns application-specific information regarding the MAYAChain state machine, such as balances, transactions and more. Careful querying this too much - you could overload the public nodes. Consider running your own node.&#x20;

**Runs on every Node**

Port: `1317`

RPC Guide:\
http://\<host>:1317/mayachain/doc/

**Mainnet**

* [https://mayanode.mayachain.info/mayachain/doc](https://mayanode.mayachain.info/mayachain/doc)

**Stagenet**

* [https://stagenet.mayanode.mayachain.info/mayachain/doc](https://stagenet.mayanode.mayachain.info/mayachain/doc)

### Cosmos RPC

The Cosmos RPC allows Cosmos base blockchain information to be returned.  However, not all endpoints have been enabled.\
\
**Endpoints guide**

[https://v1.cosmos.network/rpc/v0.45.1](https://v1.cosmos.network/rpc/v0.45.1)

**Example URL**\
[https://stagenet.mayanode.mayachain.info/cosmos/bank/v1beta1/balances/smaya18z343fsdlav47chtkyp0aawqt6sgxsh3ctcu6u](https://stagenet.mayanode.mayachain.info/cosmos/bank/v1beta1/balances/smaya18z343fsdlav47chtkyp0aawqt6sgxsh3ctcu6u)

### Tendermint RPC

The Tendermint RPC allows Tendermint consensus information to be returned.

**Any Node Ports**

* MAINNET Port: `27147`
* STAGENET Port: `26657`
* TESTNET Port: `26657`

**Endpoints guide**\
\
[https://docs.tendermint.com/master/rpc/#/](https://docs.tendermint.com/master/rpc/#/)

**`URLs` (for post-hard-fork blocks 4786560 and later)**

[https://tendermint.mayachain.info](https://tendermint.mayachain.info)

[https://stagenet.tendermint.mayachain.info](https://stagenet.tendermint.mayachain.info)

### **P2P**

P2P is the network layer between nodes, useful for network debugging.

MAINNET Port: `27146`

STAGENET Port: `27146`

P2P Guide\
[https://docs.tendermint.com/master/spec/p2p/](https://docs.tendermint.com/master/spec/p2p/)
