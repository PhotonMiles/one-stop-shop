---
description: Interfaces need to monitor and react to keep network paramaters.
---

# Network Halts

\
If the network is halted, do not send funds. The easiest check to do is if `halted = true` on the inbound addresses endpoint.In most cases funds won't be lost if they are sent when halted, but they may be significantly delayed.In the worse case if MAYAChain suffers a consensus halt the `inbound_addresses` endpoint will freeze with `halted = false` but the network is actually hard-halted. In this case running a fullnode is beneficial, because the last block will become stale after 6 seconds and interfaces can detect this.Interfaces that provide LP management can provide more feedback to the user what specifically is halted.There are levels of granularity the network has to control itself and chains in the event of issues. Interfaces need to monitor these settings and apply appropriate controls in their interfaces, inform users and prevent unsupported actions.All activity is controlled within [Mimir](https://midgard.mayachain.info/v2/mayachain/mimir) and needs to be observed by interfaces and acted upon. Also, see a description of [Constants and Mimir](https://docs.mayachain.org/network/constants-and-mimir).Halt flags are Boolean. For clarity **** `0` = false, no issues and `> 0` = true (usually 1), halt in effect.

#### Halt/ Pause Management <a href="#halt-pause-management" id="halt-pause-management"></a>

Each chain has granular control allowing each chain to be halted or resumed on a specific chain as required. Network-level halting is also possible.

1. 1.**Specific Chain Signing Halt** - Allows inbound transactions but stops the signing of outbound transactions. Outbound transactions are [queued](https://mayanode.mayachain.info/mayachain/queue). This is the least impactful halt.
   1. 1.Mimir setting is `HALTSIGNING[Chain]`, e.g. `HALTSIGNINGBNB`
2. 2.**Specific Chain Liquidity Provider Pause -** addition and withdrawal of liquidity are suspended but swaps and other transactions are processed.
   1. 1.Mimir setting is `PAUSELP[Chain]`, e,g, `PAUSELPBCH`for BCH
3. 3.**Specific Chain Trading Halt** - Transactions on external chains are observed but not processed, only [refunds ](https://dev.mayachain.org/mayachain-dev/concepts/memos#refunds)are given. MAYANode's Bifrost is running, nodes are synced to the tip therefore trading resumption can happen very quickly.
   1. 1.Mimir setting is `HALT[Chain]TRADING`, e,g, `HALTBCHTRADING`for BCH
4. 4.**Specific Chain Halt** - Serious halt where transitions on that chain are no longer observed and MAYANodes will not be synced to the chain tip, usually their Bifrost offline. Resumption will require a majority of nodes syncing to the tip before trading can commence.
   1. 1.Mimir setting is `HALT[Chain]CHAIN`, e,g, `HALTBCHCHAIN` for BCH.

Chain specific halts do occur and need to be monitored and reacted to when they occur. Users should not be able to send transactions via an interface when a halt is in effect.

#### **Network Level Halts** <a href="#network-level-halts" id="network-level-halts"></a>

**Network Pause LP** `PAUSELP` = 1 Addition and withdrawal of liquidity are suspended for all pools but swaps and other transactions are processed.**Network Trading Halt** `HALTTRADING = 1` will stop all trading for every connected chain. The MAYAChain blockchain will continue and native CACAO transactions will be processed.There is no Network level chain halt setting as the MAYAChain Blockchain continually needs to produce blocks.A chain halt is possible in which case Mimir or Midgard will not return data. This can happen if the chain suffers consensus failure or more than 1/3 of nodes are switched off. If this occurs the Dev Discord Server `#interface-alerts` will issue alerts.While very rare, a network level halt is possible and should be monitored for.

#### Synth Management <a href="#synth-management" id="synth-management"></a>

Synths minting and redeeming can be enabled and disabled using flags. There is also a Synth mint limit. The setting are:

* `MINTSYNTHS` controls minting
* `BURNSYNTHS` controls Synth Burning
* `MAXSYNTHPERASSETDEPTH` - controls the asset depth limit for each pool

#### ILP Management <a href="#ilp-management" id="ilp-management"></a>

ILP is managed by the integer setting `FULLIMPLOSSPROTECTIONBLOCKS`. If it = 0, it is disabled.See also [Constants and Mimir](https://docs.mayachain.org/network/constants-and-mimir).
