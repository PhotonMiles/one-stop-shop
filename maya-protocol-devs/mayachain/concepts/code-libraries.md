---
description: The following libraries will help you in your integration
---

# Code Libraries

### XCHAINJS

Started as a wallet library for creating keystores, getting balances and history, as well as signing and broadcasting transactions but has now expanded as a one-stop shop for MAYAChain functionality. \
[https://docs.xchainjs.org/overview/](https://docs.xchainjs.org/overview/)\
[https://github.com/xchainjs/xchainjs-lib](https://github.com/xchainjs/xchainjs-lib)
