---
description: How to join MAYAChain as an Node.
---

# Joining

## **Joining MAYAChain**

Now that you have a MAYANode deployed in your Kubernetes cluster, you need to start operating your node to join the network.

There are a couple of steps to follow to do so.

### 1. Add Liquidity

MayaChain nodes are backed up by added liquidity, just as a normal liquidity provider. Once your liquidity reaches the minimum bond, you will be able to use it as such. To know if you have sufficient cacao just multiply your cacao deposit value times two to take both your cacao and asset into account.

### 2. Check your current node status

The first step would be to make sure your deployment was successful and your node is running correctly. \_\*\*\_To check the current status of your node, you can run the command status from the `node-launcher`repository on your terminal:

```
make status
```

You will get an output along those lines, the example below is for a testnet node:

```
   __  ________  _____   _  __        __   
  /  |/  /   \ \/ /   | / |/ /__  ___/ /__ 
 / /|_/ / /| |\  / /| |/    / __\/ __ / -_)
/_/  /_/_/ |_|/_/_/ |_/_/|_/\___/\_,_/\__/ 

ADDRESS     smaya1gfy92dnp0jjs5q9tdnfz9kwwsluasn3durgyl3
IP          18.216.57.25
VERSION     1.96.1
STATUS      Active
BOND        100,000.00
REWARDS     0.00
SLASH       0
PREFLIGHT   {
  "status": "Ready",
  "reason": "OK",
  "code": 0
}

API         http://18.216.57.25:1317/mayachain/doc/
RPC         http://18.216.57.25:27147
MIDGARD     http://18.216.57.25:8080/v2/doc

CHAIN      SYNC       BEHIND         TIP       
MAYA       100.000%   0              22,749    
MAYA       100.000%   0              9,809,100 
BNB        Error      -300,254,234   300,254,234
BTC        100.000%   0              779,178   
ETH        100.000%   0              16,750,634
LTC        100.000%   0              0         
BCH        100.000%   0              0     
```

Your node is running but as you can see in the \`Preflight\` section, your node is not yet ready to be churned in and currently is in standby status, since your node has no IP address setup yet.

But to be able to set up the node IP address, you first need to get it registered in the chain by sending your BOND.

{% hint style="warning" %}
Before sending the BOND, verify that your MAYANode is **fully synced** with connected chains. Connected chains such as Ethereum & Bitcoin may take a day to sync. If your node is fully bonded and is selected to churn into MAYAChain as ACTIVE without fully syncing all connected chains, you will immediately get slashed for missing observations and lose money. It is normal to see Ethereum sit on 99.999% for many hours - be patient.
{% endhint %}

### 3. Bond your liquidity

1\) You will do a "Deposit" transaction using native MAYAChain CACAO. This is an internal MsgDeposit transaction (different from a MsgSend to another wallet). There is no destination address. The Bond is locked in the pool without the ability to withdraw it.

2\) Deposit your BOND using the memo `BOND:<mayanode-address>`. You can install the mayanode cli or use docker

{% hint style="info" %}
If you use docker take into account you'll have to use `--keyring-backend file on each of the following commands of mayanode` and map a volume from your local `~/.mayanode` to `/root/.mayanode` in the container.
{% endhint %}

**Install mayanode cli**

1. [Install](https://go.dev/doc/install) `go 1.20+`
2. Clone the repository\
   `$ git clone https://gitlab.com/mayachain/mayanode`
3. Install [docker](https://docs.docker.com/engine/install/)
4. Install [protobuf](https://grpc.io/docs/protoc-installation/)
5. Change to the branch develop if not already in it\
   `$ git checkout develop`
6. Build the binary\
   `$ TAG=mainnet make build && TAG=mainnet make install`
7. To add a mnemonic that you already own

```bash
$ mayanode keys add [key name] --recover
> Enter your bip39 mnemonic
[paste/write your mnemonic]
```

8. You’re done! You can verify with the following command:&#x20;

<pre><code><strong>$ mayanode keys show [key name]
</strong><strong>- name: mywallet
</strong>  type: local
  address: tmaya1gp27t2hcj603p8448qwkhfh0v4y0r3jkmdgsyn
  pubkey: '{"@type":"/cosmos.crypto.secp256k1.PubKey","key":"A0LirKOHGVq0y1JBcadN10UvqtnvYNvg8QOn4Mj5Gy31"}'
  mnemonic: ""
</code></pre>

{% hint style="info" %}
Some `make` commands during setup require CACAO (0.2 to 1.0) to execute into the state machine to prevent DDoS.
{% endhint %}

Once you have the docker image or the cli installed you can run\
`$ mayanode tx mayachain deposit 10000000000 cacao bond:[node-address] --from [key-name] --chain-id mayachain-mainnet-v1 --gas auto --node tcp://[ip-of-node]:27147`

Give the network 3-5 seconds to pick up your bond. To verify it has received your bond, run the following:

```
curl https://mayanode.mayachain.info/mayachain/node/<node-address>
```

If you run `make status` again, you should see this:

```
 __  ________  _____   _  __        __   
  /  |/  /   \ \/ /   | / |/ /__  ___/ /__ 
 / /|_/ / /| |\  / /| |/    / __\/ __ / -_)
/_/  /_/_/ |_|/_/_/ |_/_/|_/\___/\_,_/\__/ 
ADDRESS     maya13hh6qyj0xgw0gv7qpay8thfucxw8hqkved9vr2
IP
VERSION     0.0.0
STATUS      Whitelisted
BOND        1000.00
REWARDS     0.00
SLASH       0
PREFLIGHT   { "status": "Standby", "reason": "node account has invalid registered IP address", "code": 1 }
API         http://:1317/mayachain/doc/
RPC         http://:26657
MIDGARD     http://:8080/v2/doc
CHAIN       SYNC       BLOCKS
MAYA        55.250%    37,097/67,144
BNB         99.915%    155,426,471/155,559,013
BTC         2.399%     227,986/678,340
ETH         7.764%     947,182/12,199,994
LTC         0.012%     6,526/1,818,000
BCH         2.293%     197,340/682,404
```

As you can see, it is in standby but does not have an IP registered yet. This is needed for peer discovery.

### 4. Setup Node IP Address

You must tell MAYAChain your IP-Address for its address book and seed-service to run properly:

```
make set-ip-address
```

If you run the status command again, you should now see a different message for the Preflight section saying you need to set your node keys.

{% hint style="info" %}
Once your IP address has been registered for discovery, you can use your own host for queries.
{% endhint %}

### 5. Setup Node keys

Tell MAYAChain about your public keys for signing sessions:

```
make set-node-keys
```

If you run the `make status` command again, you should now see that your node is in status “ready” and is now ready to be churned in the next rotation.

### 6. Set Version

Make sure your node broadcasts its latest version, else you won't churn in since MAYAChain enforces a version requirement. This version will appear in your `make status`. If you are on `0.0.0` then you haven't set your version:

```
make set-version
```

## Bonding The Right Amount

Although your node is ready to be churned in, it doesn’t mean it will be the next one to be selected, since someone else could have posted a higher bond than you. To maximize chances of a quick entry, monitor Midgard to see what everyone else is bonding and try to outbid them. Keep an eye on `maximumStandbyBond` and make sure you are bonding higher than that amount.

```
curl http://52.221.153.64:8080/v2/network | json_pp

resp:
 "bondMetrics" : {
      "minimumActiveBond" : "10001000000000",
      "medianStandbyBond" : "1010000000000",
      "medianActiveBond" : "15001000000000",
      "averageStandbyBond" : "1010000000000",
      "maximumActiveBond" : "15001000000000",
      "averageActiveBond" : "12006800000000",
      "maximumStandbyBond" : "1010000000000",
      "totalStandbyBond" : "1010000000000",
      "totalActiveBond" : "60034000000000",
      "minimumStandbyBond" : "1010000000000"
   }
```

The endpoint will show data on average, median, total, minimum and maximum bond amounts. For fastest entry, bond higher than the current maximum.

{% hint style="info" %}
CACAO is always displayed in 1e8 format, 100000000 = 1 CACAO
{% endhint %}

### Bonding More

At any time during standby or active, you can bond more by adding more liquidity to your LP position

{% hint style="info" %}
Only the original wallet that did the first BOND will be able to LEAVE/UNBOND. You can top up BOND using a different wallet but make sure you keep the private key to the original BOND wallet secure and accessible.
{% endhint %}

{% hint style="danger" %}
You cannot unbond some part of your bond. The rewards for you node will be given directly to your `bond_address` when you churn out.
{% endhint %}

## Genesis Nodes

Genesis nodes are the ones responsible for a secure and fair launch. To ensure that genesis nodes will be just a tool for it, the following process will be used:

1. Genesis nodes' organizations will create and provide a Maya address.
2. LP positions of these addresses will be added to the genesis with their cacao side set to the minimum for them to bond.
3. A whitelist will be created containing these addresses, which will prevent them from withdrawing normally.
4. Once other nodes start to churn in, genesis nodes will LEAVE progressively.
5. When a genesis node withdraws, his LP position will be cleared and all the cacao will be sent to the reserve.

#### Create Maya address and mnemonic

1. [Install](https://go.dev/doc/install) `go 1.18+`
2. Clone the [Maya repository](https://gitlab.com/mayachain/mayanode)
3. Install [docker](https://docs.docker.com/engine/install/)
4. Install [protobuf](https://grpc.io/docs/protoc-installation/)

`$ git clone https://gitlab.com/mayachain/mayanode`

5. Change to the `genesis-nodes` branch

`$ git checkout genesis-nodes`

6. Build the binary

`$ TAG=mainnet make build && TAG=mainnet make install`

7. To add a mnemonic that you already own

```bash
$ mayanode keys add [key name] --recover
> Enter your bip39 mnemonic
[paste/write your mnemonic]
```

8. Send your address to the [genesis-nodes channel](https://discordapp.com/channels/915368890475880468/1068969841647693884).
