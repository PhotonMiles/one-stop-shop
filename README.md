---
description: >-
  Here are our 4 timelines that cover from the first steps of the Liquidity
  Auction to what is coming for 2024.
---

# Roadmap

<figure><img src=".gitbook/assets/WhatsApp Image 2023-03-16 at 17.46.52.jpeg" alt=""><figcaption></figcaption></figure>

The 1st $RUNE snapshot occurred on March 6th. The LA process happens from March 7 until March 30. Afterward, $CACAO donation to pools and trading will be enabled.&#x20;

We're changing the 2% $MAYA allocation model from being dependent on first inflows to pools. While it generated an incentive to participate early, people could have rushed in, causing losses. To solve this, we're changing to a time-dependent allocation model. The first 1.5% of the Maya Award will be given to T1 liquidity added from March 11th to 15th, and the remaining 0.5% from March 15th to 19th.&#x20;

The awards per pool are:

1. 7,000 $MAYA for BTC Pool
2. 7,000 $MAYA for ETH Pool
3. 2,000 $MAYA for RUNE Pool
4. 2,000 $MAYA for USDT Pool
5. 2,000 $MAYA for USDC Pool

So if you add $10,000 USDC to T1 during the first award period and there were $500,000 T1 Liquidity raised for that pool during that period, you get 2,000 $MAYA x 75% x $10,000/$500,000 = 30 $MAYA.

<figure><img src=".gitbook/assets/timeline final verde (1).png" alt=""><figcaption></figcaption></figure>

Next, let's take a look at our 2023 plans. Lockup periods vary by tier, with Tier 1 lasting 200 days, Tier 2 lasting 90 days, and Tier 3 lasting 30 days. The tentative launch date for Aztec is mid-May. The last (42nd) $RUNE snapshot will be taken on April 17th, which is also the last day to claim $MAYA.

<figure><img src=".gitbook/assets/timelines-03 finales-02.png" alt=""><figcaption></figcaption></figure>

Finally, a broader picture of upcoming events. The Early Nodes Airdrop will distribute 1.75% of $MAYA tokens each time. ILP coverage starts on May 17th for Auction LPs. The Tier 1 airdrop will occur on 3 different dates, with 1.66% of $MAYA tokens being distributed each time.

<figure><img src=".gitbook/assets/timelines-03 finales-01.png" alt=""><figcaption></figcaption></figure>
