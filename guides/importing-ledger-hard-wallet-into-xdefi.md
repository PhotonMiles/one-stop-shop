# Importing Ledger hard wallet into XDEFI

1. Create a new XDEFI wallet, or use an existing one. Make sure to “Prioritize XDEFI” in the settings.

<figure><img src="../.gitbook/assets/ledger into xdefi 1.png" alt=""><figcaption></figcaption></figure>

2. Import Ledger accts into XDEFI via Settings > Wallet management > Connect to the hardware wallet. This opens a new screen.

<figure><img src="../.gitbook/assets/ledger into xdefi 2.png" alt=""><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/ledger into xdefi 3.png" alt=""><figcaption></figcaption></figure>

3. Plug the Ledger into the computer’s USB port, enter the PIN code to unlock, and select the chain app on the Ledger device you want to open (ex., Bitcoin app).

<figure><img src="../.gitbook/assets/ledger into xdefi 4.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
If you get this type of error you can find a debug checklist for importing Ledger into XDEFI at the end of this guide.
{% endhint %}

<figure><img src="../.gitbook/assets/8B0AB143-2115-4650-8AFF-5581E5B7DD0F.png" alt=""><figcaption></figcaption></figure>

4. In the browser, select the addresses from that chain to import.

<figure><img src="../.gitbook/assets/ledger into xdefi 6 (1).png" alt=""><figcaption></figcaption></figure>

5. Once complete, go back to XDEFI and repeat the process to add hardware wallets to XDEFI from other Ledger chains.
6. Make sure to quit the Ledger device app first, then open the chain app on Ledger you’re adding next.

<figure><img src="../.gitbook/assets/ledger into xdefi 7.png" alt=""><figcaption></figcaption></figure>

{% hint style="info" %}
When you open THORChain app on your Ledger, the first screen says “Pending Ledger Review” - double-click past this, so the Thorchain app is open and ready.
{% endhint %}

### Debugging process for Ledger devices&#x20;

* [x] Make sure to use the latest XDEFI Wallet version, Updates are automatic. However, you can also close and reopen your web browser or open your extensions manager, activate the developer mode and click on the "Update' button.
* [x] Open LEDGER Live and update the firmware.
* [x] In LEDGER Live, open 'My Ledger', uninstall (from the 'Apps installed' tab) and reinstall (from the 'App catalog' tab) the dedicated app of the blockchain you want to interact with.
* [x] Close Ledger Live and all other processes using your LEDGER device.&#x20;
* [x] Disconnect your LEDGER device from your desktop/laptop and reconnect it to your computer.
* [x] Enter your LEDGER PIN code from your hardware wallet device.
* [x] From your LEDGER device, open the app dedicated to the blockchain you want to interact with.
* [x] Still from your LEDGER device and for the Ethereum network, you may need to open 'Settings' and enable the 'Blind signing' option. Please read closely the [official LEDGER documentation](https://www.ledger.com/academy/enable-blind-signing-why-%20when-and-how-to-stay-safe).
* [x] If you still encounter the same issue, please save everything you need and clear your web browser History via the advanced tab (scroll up/down to select all data types, time range: all time).
* [x] End your web activities and close all open web browser tabs.
* [x] Close your web browser (kill the process) and reopen it.

