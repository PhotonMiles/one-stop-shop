# Withdrawing liquidity from Tier 2 or 3 using Asgardex

**1. Setup**

Open and login to Asgardex just as you have for adding liquidity.

**2. Withdrawing Liquidity**&#x20;

Under the WALLET tab, choose ASSETS and select the appropriate row corresponding to the Ledger account to wish to provide from.

Click the “Action” dropdown and choose the SEND option. It may not seem intuitive, but we are just using this to send a memo for withdrawal.

**3. Send Address field**

The Address field will use the same address for both PROVIDING or WITHDRAWING.

In the Address field, enter the same address you used (above) for providing liquidity corresponding to that pool's token.

{% hint style="success" %}
Inbound addresses [here](https://mayanode.mayachain.info/mayachain/inbound\_addresses)!
{% endhint %}

**6. Amount field**

In the Amount field, enter an amount larger than the Estimated Fee. It can be minimal because this is not determining the amount you withdraw from the auction (the Memo will determine the amount). This number just needs to be higher than the estimated fees.

**7. Memo field**&#x20;

The text string entered in the Memo field will specify that it's a withdrawal, the token type, the percent amount you're withdrawing, which side of the symmetrical pair you're withdrawing to, and your Maya wallet address.&#x20;

Again, the format is specific, so please use the guide below:

Memo field input (by the pool) when WITHDRAWING

A. Change out "mayaXXX..." with your Maya wallet address

B. The "10000" represents 100% of what you would like to withdraw. For 50% withdrawal, you would enter "5000," or 25% would be "2500," etc.

C. Input as shown. Make sure no spaces

| RUNE | wd:thor.rune:10000:thor.rune:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |
| ---- | ------------------------------------------------------------------- |
| BTC  | wd:btc.btc:10000:btc.btc:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     |
| ETH  | wd:eth.eth:10000:eth.eth:mayaXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX     |

**8. Complete tx**&#x20;

When it's all filled in, click the Send button. Enter your password to complete the tx

\*It's recommended to use the “Fast” or “Fastest” option for sending to ensure the tx doesn't get hung up over gas changes on the chain.&#x20;

When it's complete, check the tx to ensure it was successful.

\
