# How to claim your $MAYA as a $RUNE holder

$RUNE holders are eligible for the $MAYA tokens airdrop, as outlined [here](../maya-whitepaper-2.0/usdmaya-token/technical-overview-mt.md). In order for us to know where to distribute the tokens, eligible holders need to send at least 1 $RUNE the the Maya Rune pool.

You don't have to leave your $RUNE in our pools or in the Liquidity Auction if you don't want to. Once the deposit has been successful, your THOR and your MAYA addresses will be linked, and you can then choose to withdraw your tokens if you'd rather.&#x20;

Here are the steps to claim your tokens:

1. **Get a MAYA address!** To this moment, there are three different options:

a. The [THORWallet DEX mobile wallet App](https://linktr.ee/THORWallet) will automatically create a MAYA address for you (on both, iOS and Android). Make sure that your THORWallet App is updated to the latest version!

b. The THORWallet webapp, with a keystore, will automatically create a MAYA address for you.&#x20;

c. You can use the mayanode cli.

{% hint style="info" %}
MAYA addresses will have the format: mayaxxxxxxxxxxxxx
{% endhint %}

2. **Link your Rune address to your Maya address.**

In order to claim your $MAYA tokens you must send at least 1 $RUNE from your eligible THOR wallet to the $RUNE Maya pool. This process helps us know which MAYA address belongs to which RUNE holder and distribute the correct amount of tokens. Send your $RUNE using one of the following methods:

a. Use the THORWallet mobile App or webapp with Keystore. Check the "[Add liquidity through THORWallet](broken-reference)” guide

b. Use the [THORWallet webapp](https://app.thorwallet.org/) with XDEFI.

c. Via Asgardex.

d. Using the mayacode cli.

{% hint style="warning" %}
April 17th is the last day to link your MAYA address to your THOR address. Please make sure to complete this step before said date.
{% endhint %}

3. **Receive your $MAYA tokens!**

Your $MAYA tokens will be automatically airdropped into your new MAYA address before April 26th based on the RUNE positions held in the linked THOR addresses and the [drop criteria](../maya-whitepaper-2.0/usdmaya-token/technical-overview-mt.md).

{% hint style="success" %}
To confirm that we will see your deposit, enter your MAYA address into the [Maya Explorer](https://www.explorer.mayachain.info/transactions). You should see your transaction marked with either "AddLiquidity | pending" or "Refund | Success".
{% endhint %}

### **Users holding $RUNE in other wallets (e.g. Ledger, Trust Wallet, etc).**

{% hint style="danger" %}
TRUST WALLET IS NOT WORKING AT THE MOMENT
{% endhint %}

The process to claim your $MAYA is similar: you need to create a MAYA wallet address (by following Step #1 above) and then link it to your THOR address by sending at least 1 $RUNE to the corresponding Maya pool.

However, initiating the send transaction from your current wallet means it will not include the necessary information in the memo field. To avoid this, you can either:

a. Temporarily import your wallet's seed phrase into THORWallet and let the app handle the construction of the memo and the subsequent transaction for you (Step #2, above).

b. Manually construct a transaction memo in the custom memo field in Asgardex or Thorswap and send the transaction with it. See more below.

{% hint style="warning" %}
Importing your hardware wallet seed phrase into THORWallet is strongly discouraged.
{% endhint %}

### Claiming using manual transaction memos (e.g. Asgardex)&#x20;

If you are knowledgeable about the TC / MAYA architecture and how vault addresses work - including churning details during / after the Liquidity Auction - you can build a custom memo and transaction like so:

1. Send at least 1 $RUNE to the Maya TC / RUNE Asgard address (address format is thor1xxxxxxxxxxxxxx).
2. Importantly, the memo should look like this: +:thor.rune:maya1xxxxxxxxxxx , where maya1xxxxxxxxxxxxxx is the user’s MAYA address.
3. Make sure to use SEND and not DEPOSIT.&#x20;

{% hint style="warning" %}
Maya TC / RUNE Asgard addresses change recurrently. Sending $RUNE, or any other coins, into older vaults will result in a permanent loss of funds - and will not get you your airdrop either!



If you want to confirm the current Inbound Addresses, you can [find them here](https://mayanode.mayachain.info/mayachain/inbound\_addresses).
{% endhint %}

### Linking more than one THOR address to a single Maya address

Linking two THORChain addresses (e.g. address **A** and address **B**) with one Maya address (e.g. address **M**) would look like this, and result in the following:

1. You send 1 $RUNE from address **A** to CACAO/RUNE pool with the address of **M** in memo to link address **A** and **M**.
2. You send 1 $RUNE from address **B** to CACAO/RUNE pool with the address of **M** in memo to link address **B** and **M**.

Address **A's transaction would be considered a LA deposit**, and address **B's would be used for the purpose of the $MAYA airdrop** link. Only one **M**aya address can be linked to an external asset's address for LA purposes and therefore, address **B** would no longer be able to enter the LA successfully (since address **A** was already linked to address **M**).

{% hint style="danger" %}
If you wanted to add more than 1 $RUNE to the Liquidity Auction, you would do that with the first address (**A**). If you did with the second address (**B**), all $RUNE would be refunded instead (and would not participate in the LA). In other words, those would be only linked for the purpose of the airdrop.

Regardless of the case, enough information would be available for us to find you, count your $RUNE, and send your $MAYA to the indicated address.&#x20;
{% endhint %}
