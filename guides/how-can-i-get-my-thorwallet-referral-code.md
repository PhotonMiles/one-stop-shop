---
description: 'The process is simple and straightforward. Here are the steps:'
---

# How can I get my THORWallet referral code?

1. Inside your ThorWallet app, tap the “Missions” icon.

<img src="../.gitbook/assets/IMG_7319.PNG" alt="" data-size="original">

2. Find the “Your code” button and tap on it.

![](../.gitbook/assets/IMG\_7320.PNG)

4. THORWallet will generate a small text and url with your referral code included. Share it on Twitter or tweak it and send it however you want.

{% hint style="success" %}
Download [THORWallet](https://linktr.ee/THORWallet) and earn 5% of fees in $TGT 🔥

🚨The only non-custodial wallet to swap native crypto assets across chains and earn passive income on them.

Click [here](https://app.thorwallet.org/referrals?code=763386)!

\#ReferTHORWallet
{% endhint %}
