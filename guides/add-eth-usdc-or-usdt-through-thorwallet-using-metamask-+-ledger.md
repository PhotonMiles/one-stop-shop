# Add ETH, USDC, or USDT through THORWallet using Metamask + Ledger

### Importing ETH account from ledger into Metamask

1. Open MetaMask extension. Make sure it is updated to version 10.5.0 or later.
2. Open “Advanced Settings” in MetaMask and check that “Preferred Ledger Connection Type” is set to “WebHID.”
3. Under MetaMask “My Accounts” in the upper right of the extension, select “Connect Hardware Wallet.”

![](<../.gitbook/assets/ledger thorwallet 1 (1) (1).png>)

4. Select Ledger. Connect Ledger to the computer, unlock Ledger, and open the Ethereum app on it.

![](<../.gitbook/assets/ledger thorwallet 2.png>)

5. Make sure to enable "smart contract data" or "blind signing" on the Ledger device. You’ll need this for signing on THORWallet.
6. Click Continue in the browser. A popup will appear where you have to select your device to pair.

![](<../.gitbook/assets/ledger thorwallet 3 (1).png>)

7. Select the ETH account you wish to import into MetaMask (Ledger Live worked in testing).

![](<../.gitbook/assets/ledger thorwallet 4.png>)

7. Click Unlock. If you get an error, make sure to unlock your Ledger device again, then retry.
8. This should have now imported your Ledger’s ETH account into MetaMask.

### Connecting to THORWallet

**Go to** [**app.thorwallet.org**](https://app.thorwallet.org/) **and click Connect.**

If the first time connecting and setting up your Ledger and MetaMask for the Liquidity Auction, you need to create a keystore and associated password that will have a **SEPARATE** seed phrase from your Ledger device. This keystore will contain the Maya address necessary to connect to the ETH address in your Ledger whose tokens you’re providing to one of the pools (ETH, USDC, USDT).&#x20;

Not to worry, the process is simple and streamlined.

1. In the Connect screen, accept the Terms and select the top connection choice named “MetaMask + Supports Maya LA”.

![](<../.gitbook/assets/ledger thorwallet 5.png>)

2. Select the Create Keystore option.

![](<../.gitbook/assets/ledger thorwallet 6.png>)

3. Choose a keystore password and confirm it. Copy down this seed phrase for safekeeping in case you misplace your keystore file and keystore password later.

![](<../.gitbook/assets/ledger thorwallet 7.png>)

4. Click Download and store the “thorwallet-keystore.txt” file somewhere secure that you have access to. You’ll need access to it and the keystore password.
5. Now that you have a keystore file you can connect to your Ledger account in MetaMask in unison with it.
6. Click Connect, accept the Terms, and select the top connection choice named “MetaMask + Supports Maya LA”.
7. Select the Import Keystore option. Select the Keystore file and the Decryption Password you created for it. Click Unlock.
8. Your Ledger is now connected through MetaMask with the associated keystore file that contains your maya address.&#x20;

{% hint style="info" %}
You will not see your maya address when connected with the MetaMask/Ledger option as THORWallet is displaying the Ledger wallet only. To locate your maya address, simply use bottom Keystore import option for connecting to connect and view your maya address from that wallet.
{% endhint %}

### Providing Liquidity

1. Make sure you’re connected to THORWallet with the MetaMask/Ledger option. Select “MAYA Liquidity Auction from the left tab. Select which Ethereum pool (ETH, USDT, USDC) you wish to add liquidity to.
2. Select a TIER and an AMOUNT you wish to provide. Make sure you have plenty of ETH to cover gas costs.

![](<../.gitbook/assets/ledger thorwallet 8 (1).png>)

3. As it’s your first time providing this token to THORWallet there may be an extra Approve token Contract step to allow the Smart Contract to execute the contract.
4. Make sure your Ledger device is unlocked and Approve Contract.

![](<../.gitbook/assets/ledger thorwallet 9 (2).png>)

5. A MetaMask Approve screen will popup with may have a Spending Cap box. Click on MAX, which is the default for token approvals. If you enter a small custom spending cap, you may get stuck in approvals and not be able to complete adding liquidity.

![](<../.gitbook/assets/ledger thorwallet 10 (2).png>)

5. Select Preview to see the Overview. Leave the setting on “Urgent” to ensure the tx goes through. Check the box and click “Add Liquidity Now” when ready to proceed.

![](<../.gitbook/assets/ledger thorwallet 12.png>)

5. A screen will popup in MetaMask asking you to Confirm the transaction. Make sure your Ledger device is unlocked and in the Ethereum app before clicking Confirm.
6. Once you click Confirm your Ledger device will need to “Verify Fields” with several approvals and a final Approve + Sign screen. You will need to click through the screens and approve each of them for the full transaction confirmation. There could be as many as 9 fields before the final Approve and Sign screen. This is Ledger’s approval of all the information in the Confirm screen you are signing to provide liquidity.
7. You will now see the Pending Transaction. Allow it ample time to complete. You can click “Follow Transaction” to view the confirmation on Etherscan. Once it’s complete, you’ll notice it displays two stacked transactions. The larger of the two is the Etherscan tx.

![](<../.gitbook/assets/ledger thorwallet 13 (1).png>)

### Check your pool position

Select My Pools tab on the left. If you don’t see your position, switch to display the Maya pools. Your pool positions should all appear here.

![](<../.gitbook/assets/ledger thorwallet 14.png>)

### Pool withdrawals

If you provided to Tier 2 or Tier 3, you could withdraw from the pool anytime during the Liquidity Auction. To do so:

1. Select the My Pools tab on the left. If you don’t see your position, switch to display the Maya pools. Your pool positions should all appear here.
2. Select Withdraw on the right. This will withdraw your full position. Click Preview.
3. This will show an Overview. Click the checkbox and Withdraw Liquidity.

![](<../.gitbook/assets/ledger thorwallet 15.png>)

4. A screen will pop up in MetaMask asking you to Confirm the transaction. Ensure your Ledger device is unlocked and in the Ethereum app before clicking Confirm.

![](<../.gitbook/assets/ledger thorwallet 16.png>)

5. Once you click Confirm, your Ledger device must “Verify Fields” with several approvals and a final Approve + Sign screen. You will need to click through the screens and approve each of them for the full transaction confirmation. There will be several fields to review and sign before the final Approve and Sign screen. This is Ledger’s approval of all the information in the Confirm screen you are signing to withdraw liquidity.
6. You will now see the Pending Transaction. Allow it ample time to complete. You can click “Follow Transaction” to view the confirmation on Etherscan. Once it’s complete, you’ll notice it displays two stacked transactions.

![](<../.gitbook/assets/ledger thorwallet 17.png>)

5. Verify that your liquidity has been removed from the pool by clicking on My Pools on the left.
6. The funds minus router fees should return to your wallet within the hour, sometimes much quicker.

### Router fees

{% hint style="info" %}
There are high router fees for this withdrawal that are not affiliate fees but on-chain router fees. Be aware that these fees are subtracted from your provided amount so your withdrawal will be less than your current amount provided.&#x20;
{% endhint %}

If ETH gas fees (check ether scan’s gas tracker) are high you may want to wait until gas prices come down to minimize the amount of router fees. In particular, USDC and USDT incur noticeably high fees when withdrawn. The good news is later, when you have CACAO, you will be able to withdraw asymmetrically to CACAO, which will incur substantially fewer fees.

{% hint style="warning" %}
Important:

* All your transactions are managed by Metamask.
* We only use the keystore wallet to get your Maya address.
* On every logout (under the settings icon), you have to go through the flow again. You should use one keystore file if you have already created one. Otherwise, you will have multiple Maya addresses.
* There is **NO CONNECTION** between Metamask and keystore. These are **2 DIFFERENT** wallets with unique addresses and phrases.
{% endhint %}
