# Derive a Maya Public Address from your Thor Public Address (they will share same seed)

1. Start [here](https://bech32.scrtlabs.com/).&#x20;
2. Write "thor" on the top left and "maya" on the top right.&#x20;
3. Write your Thor Address below "thor" and it will automatically generate the right Maya Address below "maya". You can verify first that the last 6 letters changed (sometimes coincidentally, the checksum shares a few characters with the last, so less than 6 letters may change, but there is ALWAYS a significant change).&#x20;
4. You can make a second verification by sending a small test transaction which should show up [here](https://mayanode.mayachain.info/mayachain/pool/btc.btc/liquidity\_providers) (just ctrl + f for your address).
5. You can change btc.btc to eth.eth or thor.rune on that link to check your positions.&#x20;

{% hint style="info" %}
DO NOT add stablecoins manually, those require interacting with the Router smart contract manually, and it will probably lead to failure; just use Thorwallet for that instead.
{% endhint %}
