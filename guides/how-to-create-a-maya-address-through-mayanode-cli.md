# How to create a Maya address through mayanode cli

#### Create Maya address and mnemonic

1. [Install](https://go.dev/doc/install) `go 1.20+`
2. Clone the [Maya repository](https://gitlab.com/mayachain/thornode)
3. Install [docker](https://docs.docker.com/engine/install/)
4. Install [protobuf](https://grpc.io/docs/protoc-installation/) `$ git clone https://gitlab.com/mayachain/mayanode`
5. Change to the mainnet branch `$ git checkout mainnet`
6. Build the binary`$ TAG=mainnet make build && TAG=mainnet make install`
7. To add a mnemonic that you already own

```bash
$ mayanode keys add [key name] --recover
> Enter your bip39 mnemonic
[paste/write your mnemonic]
```

8. You’re done! It should look as follows:&#x20;

<pre><code><strong>$ mayanode keys add [key name] --recover
</strong><strong>- name: mywallet
</strong>  type: local
  address: tmaya1gp27t2hcj603p8448qwkhfh0v4y0r3jkmdgsyn
  pubkey: '{"@type":"/cosmos.crypto.secp256k1.PubKey","key":"A0LirKOHGVq0y1JBcadN10UvqtnvYNvg8QOn4Mj5Gy31"}'
  mnemonic: ""
</code></pre>
