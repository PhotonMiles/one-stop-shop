---
description: Swapping Assets using the ElDorado Frontend
---

# How to Swap using  ElDorado WebApp

1- Visit [ElDorado Website](https://www.eldorado.market/) and press the "Enter El Dorado" button.

<figure><img src="../.gitbook/assets/Screenshot 2023-04-29 at 03.46.16.png" alt=""><figcaption></figcaption></figure>

2- On the top right corner press the "Connect" button to connect your wallet.

<figure><img src="../.gitbook/assets/Screenshot 2023-04-29 at 03.47.15.png" alt=""><figcaption></figcaption></figure>

3- A pop-up will appear. Choose "Connect existing keystore". If you don't have a keystore, please follow the [How to set up a new Maya wallet using ElDorado](https://docs.mayaprotocol.com/guides/how-to-set-up-a-new-maya-wallet-using-eldorado) guide.

<figure><img src="../.gitbook/assets/Screenshot 2023-04-29 at 03.48.44.png" alt=""><figcaption></figcaption></figure>

4- Type in your password, then press "Decrypt".

<figure><img src="../.gitbook/assets/Screenshot 2023-04-29 at 03.49.14.png" alt=""><figcaption></figcaption></figure>

5- Choose the pairs you want to swap (eg. BTC & Cacao), where the top field is the Token you want to swap from and the bottom field is the Token you want to swap to.&#x20;

6- Enter the amount and press swap. (Make sure you have enough balance to pay for the transaction fees).

<figure><img src="../.gitbook/assets/Screenshot 2023-04-29 at 04.01.54.png" alt=""><figcaption></figcaption></figure>

7- The page will show you the status of swap.

<figure><img src="../.gitbook/assets/Screenshot 2023-04-29 at 04.07.54.png" alt=""><figcaption></figcaption></figure>

8- Once the transaction is successful, check your wallet by pressing the "view wallet button on the top right of the page.

<figure><img src="../.gitbook/assets/Screenshot 2023-04-29 at 04.14.21.png" alt=""><figcaption></figcaption></figure>

For further assistance, please visit ElDorado's [Discord](https://discord.gg/yZebrxC82s) channel and/ or Maya Protocol's [Discord](https://discord.gg/mayaprotocol) channel.
